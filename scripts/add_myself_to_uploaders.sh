#!/bin/bash

for path in $(arriero list -f path "$@"); do
    echo $path
    (
        cd $path
        changes=0
        uploaders=$(sed -n -r '/^Uploaders:/,/^[^[:space:]]/{
                                   /^(Uploaders:|[[:space:]])/p
                               }' debian/control)
        if echo "$uploaders" | grep -q 'Maximiliano Curia'; then
            :
        else
            sed -i -r '/^Uploaders:/,/^[^[:space:]]/{
                    /^(Uploaders:|[[:space:]])/  s|([^,])\s*$|\1,|
                    /^[[:space:]]/               s|^[[:space:]]+|           |
                    /^(Uploaders:|[[:space:]])/! i \
           Maximiliano Curia <maxy@debian.org>
                }' debian/control
            changes=1
        fi

        if [ $changes -gt 0 ]; then
            dch 'Add myself to uploaders.'
            git commit -a -m 'Add myself to uploaders.'
        fi
    )
done
