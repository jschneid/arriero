#!/bin/sh
# This script is intended to be run as:
# arriero exec -x "update_deps_cmake.sh cmake_parser" [packages]

if [ $# -lt 1 ]; then
    echo "usage: $0 cmake_parser" > /dev/stderr
    exit 1
fi

cmake_parser="$1"

${cmake_parser}

status=$(git status --porcelain)
if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

wrap-and-sort -f debian/control
git add debian/control
git commit -m 'Update build-deps and deps with the info from cmake'

