#!/bin/bash

MSG='Bump kde-sc-dev-latest build dependency.'

for path in $(arriero -c ~/.config/arriero-kde4.12.conf list -f path \
                $(sed -n '/^[^# ]/s/:.*//p' ~/tmp/kde4.12)); do
    echo $path;
    (
        cd $path
        change=0
        if grep -q 'kde-sc-dev-latest ([^)]*)' debian/control && \
            ! grep -q 'kde-sc-dev-latest (>= 4:4\.12[^)]*)' debian/control;
        then
            sed -i 's/kde-sc-dev-latest ([^)][^)]*)/kde-sc-dev-latest (>= 4:4.12)/' debian/control
            change=1
        fi
        if [ $change -gt 0 ]; then
            dch "$MSG";
            git commit -a -m "$MSG"
        fi
    )
done
