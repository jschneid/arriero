#!/bin/bash
# This script is intended to be run as:
# arriero exec -x tag_uploaded.sh -f version,distribution,urgency [packages]

TAG_VERSION=$(echo $version | tr ':~' '%_')
TAG="debian/$TAG_VERSION"
DESC="$version $distribution; urgency=$urgency"

git tag -s -m "$DESC" "$TAG"
