#!/bin/bash

for path in $(arriero list -f path "$@"); do
    # echo $path
    (
        cd $path
        branch=$(git rev-parse --abbrev-ref HEAD)
        if [ "$branch" != "master" ]; then
            out=$(git rev-list --left-right --count master..."$branch")
            master=$(echo $out | sed 's/\s.*//')
            if [ "$master" -gt 0 ]; then
                echo $path
                echo $out
            fi
        fi
    )
done
