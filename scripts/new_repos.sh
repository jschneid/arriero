#!/bin/sh

path="$1"

vcs_pull="git://git.debian.org/collab-maint/cinnamon"
vcs_push="git.debian.org:/git/collab-maint/cinnamon"

vcs_upstream="https://github.com/linuxmint"

if [ -e "$path" ]; then
    echo "Directory already exists" > /dev/stderr
    exit 1
fi

name=$(basename "$path")

mkdir "$path"
cd "$path"

git init

git remote add origin "${vcs_pull}/${name}.git"
git remote set-url --push origin "${vcs_push}/${name}.git"

git remote add "$name" "${vcs_upstream}/${name}.git"

git checkout --orphan master
git reset; git commit --allow-empty -m 'Initial debian branch'

git checkout --orphan upstream
git reset; git commit --allow-empty -m 'Initial upstream branch'

git checkout --orphan pristine-tar
git reset; git commit --allow-empty -m 'Initial pristine-tar branch'

git push origin master upstream pristine-tar --tags

git branch --set-upstream-to origin/master       master
git branch --set-upstream-to origin/upstream     upstream
git branch --set-upstream-to origin/pristine-tar pristine-tar

git config remote.origin.push refs/heads/master
git config --add remote.origin.push refs/heads/upstream
git config --add remote.origin.push refs/heads/pristine-tar
git config --add remote.origin.push refs/tags/debian/*
git config --add remote.origin.push refs/tags/upstream/*

git pull --all
