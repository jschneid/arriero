#!/bin/bash
# This script is intended to be run as:
# arriero exec -x wrap_and_sort.sh [packages]

set -e

MSG='wrap-and-sort'

wrap-and-sort

status=$(git status --porcelain)

if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

git commit -a -m "${MSG}"
