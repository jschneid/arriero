#!/bin/bash
# This script is intended to be run as:
# arriero exec -x update_vcs_fields.sh [packages]

set -e

status=$(git status --porcelain)
if [ -n "${status}" ]; then
    echo 'Git repository is dirty' > /dev/stderr
    exit 1
fi

MSG='debian/control: Update Vcs-Browser and Vcs-Git fields'

sed -r -i '
/^Vcs-Browser:/{
    s|^Vcs-Browser:\s*https?://git\.debian\.org|Vcs-Browser: http://anonscm.debian.org/gitweb|
    s&^Vcs-Browser:\s*https?://anonscm\.debian\.org/(cgit/|gitweb/\?p=)&Vcs-Browser: https://anonscm.debian.org/git/&
};
/^Vcs-Git:/{
    s&^Vcs-Git:\s*(git|http)://anonscm\.debian\.org&Vcs-Git: https://anonscm.debian.org&
    \&Vcs-Git: https://anonscm.debian.org/[^g][^i][^t][^/]&{
        s|^Vcs-Git:\s*https://anonscm\.debian\.org|Vcs-Git: https://anonscm.debian.org/git|
    }
};
' debian/control

status=$(git status --porcelain)

if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

wrap-and-sort
git add debian/control
git commit -m "${MSG}"
