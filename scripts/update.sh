#!/bin/sh
# This script is intended to be run as:
# arriero exec -x "update.sh -m 'Commit message' script [args...]" [packages]

commit_message=''
for i in "$@"; do
    case "$i" in
        -m)
            shift
            commit_message="$1"
            shift
            ;;
        *)
            break
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo "usage: $0 [-m commit_message] command [args...]" > /dev/stderr
    exit 1
fi

status=$(git status --porcelain)
if [ -n "${status}" ]; then
    echo 'Git repository is dirty' > /dev/stderr
    exit 1
fi

script="$1"

"${@}"
ret=$?
if [ $ret -ne 0 ]; then
    exit $ret
fi

status=$(git status --porcelain)
if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

wrap-and-sort

if [ -z "${commit_message}" ]; then
    commit_message='Automatic update with '"$(basename "${script}")"
fi
git commit -a -m "${commit_message}"

