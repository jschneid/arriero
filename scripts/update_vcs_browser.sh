#!/bin/bash
# This script is intended to be run as:
# arriero exec -x update_vcs_fields.sh [packages]

set -e

MSG='debian/control: Update Vcs-Browser: field'

sed -r -i '/^Vcs-Browser:/{
    s|^Vcs-Browser:\s*https?://git\.debian\.org|Vcs-Browser: http://anonscm.debian.org/gitweb|
    s|^Vcs-Browser:\s*https?://anonscm\.debian\.org/gitweb/\?p=|Vcs-Browser: http://anonscm.debian.org/cgit/|
}' debian/control

status=$(git status --porcelain)

if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

git add debian/control
git commit -m "${MSG}"
