#!/bin/bash

for path in $(arriero list -f path "$@"); do
    echo $path
    (
        cd $path
        changes=0
        if [ $(cat debian/compat) -lt 9 ]; then
            echo 9 > debian/compat
            changes=1
        fi
        dhv=$(sed -n -r 's!.*\W(debhelper\s+\(>= ([^)]+)\)).*!\2!p' debian/control)
        if [ $dhv != "9" ]; then
            sed -i -r 's!(\Wdebhelper\s+\(>=) [^)]+\)!\1 9)!' debian/control
            changes=1
        fi

        if [ $changes -gt 0 ]; then
            dch 'Bump debhelper build-dep and compat to 9.'
            git commit -a -m 'Bump debhelper build-dep and compat to 9.'
        fi
    )
done
