#!/bin/sh
# This script is intended to be run as:
# arriero exec -x "update_control.sh script" [packages]

if [ $# -lt 1 ]; then
    echo "usage: $0 cmake_parser" > /dev/stderr
    exit 1
fi

script="$1"

"${@}"

status=$(git status --porcelain)
if [ -z "${status}" ]; then
    # No changes needed
    exit 0
fi

wrap-and-sort -f debian/control
git add debian/control
git commit -m 'Automatic debian/control update with '"$(basename "${script}")"

