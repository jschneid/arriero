#!/usr/bin/env python3

from collections import defaultdict
import re
import subprocess
import sys

COMMIT = 'New upstream release.'
MSG = '  * New upstream release.\n'
changelog_trail = re.compile(r'^ --')
changelog_multimaint = re.compile(r'^\s*\[\s*(\b[^]]*\b)\s*\]\s*$')
changelog_empty = re.compile(r'^\s*$')
entry = re.compile(r'  \* New upstream release\.')
TRAIL = ('', 0)

for path in subprocess.check_output(['arriero', 'list', '-f', 'path'] +
                                    sys.argv[1:], universal_newlines=True).split('\n'):
    if not path:
        continue
    print(path)

    output = subprocess.check_output(['git', 'log', 'HEAD~1..HEAD',
                                      '--oneline'], cwd=path, universal_newlines=True)
    last = re.sub('^[^\s]+\s+', '', output.rstrip('\n'))
    if last != COMMIT:
        continue

    filename = '%s/debian/changelog' % (path,)
    f = open(filename)
    lines = f.readlines()
    skip = 0

    first_block = []
    maintainers = defaultdict(set)
    maintainer = TRAIL
    entries = []

    for i, line in enumerate(lines):
        first_block.append(line)
        # print(line, end='')

        m = changelog_multimaint.match(line)
        if m:
            maintainer = (m.group(1), i)
        elif changelog_empty.match(line):
            maintainer = TRAIL
        else:
            maintainers[maintainer].add(i)

        if entry.match(line):
            entries.append((i, maintainer))

        if changelog_trail.match(line):
            break
    skip = len(first_block)

    delete = set()
    # print(maintainers)
    for e in entries:
        delete.add(e[0])
        if e[1] == TRAIL:
            continue
        maintainers[e[1]].remove(e[0])
        if not maintainers[e[1]]:
            i = e[1][1]
            delete.add(i)
            if i > 2 and changelog_empty.match(first_block[i - 1]):
                delete.add(i - 1)

    f = open(filename, 'w')
    # f = sys.stdout

    for i, line in enumerate(first_block):
        if i == 2:
            f.write(MSG)
            if changelog_multimaint.match(line) and i not in delete:
                f.write('\n')
        if i in delete:
            continue
        f.write(line)
    for line in lines[skip:]:
        f.write(line)
    f.close()

    subprocess.call(['git', 'commit', '-a', '--amend', '-m',
                     'New upstream release.'], cwd=path, universal_newlines=True)
