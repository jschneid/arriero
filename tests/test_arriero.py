#!/usr/bin/env python3
# encoding: utf-8

import io
import os
import shutil
import subprocess
import sys
import tempfile
import unittest

from unittest.mock import DEFAULT, patch

# Own imports
import arriero


def create_basic_pkg(dirname):
    debian_dir = os.path.join(dirname, 'debian')
    os.mkdir(os.path.join(dirname, 'debian'))
    # The \N{space} is used to avoid confusing the indenter
    basic_content = {
        'changelog': '''\
basic-pkg (0.1-1) UNRELEASED; urgency=low

\N{space} * Initial release.

\N{space}-- Foo Bar <foo.bar@example.com>  Thu, 01 Jan 1970 00:00:00 +0000
''',
        'compat': '9',
        'control': '''\
Source: basic-pkg
Section: misc
Priority: extra
Maintainer: Foo Bar <foo.bar@example.com>
Build-Depends: debhelper (>= 9)
Standards-Version: 3.9.6

Package: basic-pkg
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Null package used to test arriero
\N{space}Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
\N{space}incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
\N{space}nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
\N{space}Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
\N{space}eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
\N{space}in culpa qui officia deserunt mollit anim id est laborum.
''',
        'copyright': '''\
Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: basic-pkg

Files: debian/*
Copyright: 1970, Foo Bar <foo.bar@example.com>
License: CC0
\ To the extent possible under law, the person who associated CC0 with
\ this work has waived all copyright and related or neighboring rights
\ to this work.
''',
        'rules': '''\
#!/usr/bin/make -f

%:
\tdh $@
''',
        'source/format': '3.0 (quilt)',
    }
    for filename, content in basic_content.items():
        parents = filename.split('/')[:-1]
        for i, parent in enumerate(parents):
            parent_dir = os.path.join(*([debian_dir] + parents[:i + 1]))
            os.mkdir(parent_dir)
        full_filename = os.path.join(debian_dir, filename)
        with open(full_filename, 'w') as f:
            f.write(content)


def create_basic_git_pkg(dirname):
    create_basic_pkg(dirname)
    subprocess.call(['git', 'init'], cwd=dirname)
    subprocess.call(['git', 'add', '-A', '.'], cwd=dirname)
    subprocess.call(['git', 'commit', '-m', 'Initial commit'], cwd=dirname)


class TestArrieroRun(unittest.TestCase):

    def test_run(self):
        # No parameters
        argv = ['arriero']
        with self.assertRaises(SystemExit) as sys_exit, \
                patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        self.assertEqual(sys_exit.exception.code, 2,
                         'Unexpected exit code: {}'.format(
                             sys_exit.exception.code))
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Missing usage output')

        # Help
        argv = ['arriero', '--help']
        with self.assertRaises(SystemExit) as sys_exit, \
                patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        self.assertEqual(sys_exit.exception.code, 0,
                         'Unexpected exit code: {}'.format(
                             sys_exit.exception.code))
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertTrue(len(out), 'Missing help output')
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(out))


class TestArrieroList(unittest.TestCase):

    def setUp(self):
        configuration = '''
        [DEFAULT]
        basedir: /foo

        [_test_group_1]
        packages: atestpkg ztestpkg
        vcs_git: git://{{name}}
        export_dir: /nonexistent

        [_test_group_2]
        packages: atestpkg ztestpkg
        export_dir: /tmp/{{name}}/export_dir

        [ztestpkg]
        path: {path}
        depends: atestpkg

        [atestpkg]
        path: {path}
        depends: testpkg

        [testpkg]
        path: {path}
        '''
        self.config_file = tempfile.NamedTemporaryFile(
            prefix='arriero.', suffix='.conf')
        self.pkg_path = tempfile.mkdtemp(prefix='arriero.')
        self.config_file.write(
            configuration.format(path=self.pkg_path).encode('utf8')
        )
        self.config_file.flush()
        self.base_argv = ['arriero', '--config', self.config_file.name]

    def tearDown(self):
        self.config_file.close()
        if os.path.exists(self.pkg_path):
            shutil.rmtree(self.pkg_path)

    def test_list_basic_pkg(self):
        create_basic_pkg(self.pkg_path)
        argv = self.base_argv + ['list', '-F', '{branch}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{changes_file}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{dist}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'UNRELEASED\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{distribution}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'UNRELEASED\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{dsc_file}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{is_dfsg}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'False\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{is_native}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'False\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{is_merged}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'False\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{last_changelog_distribution}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{last_changelog_version}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{source_name}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'basic-pkg\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{source_changes_file}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{upstream_version}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '0.1\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{urgency}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'low\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{vcs_git}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{version}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '0.1-1\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{version_at_distribution}',
                                 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

    def test_list_basic_git_pkg(self):
        create_basic_git_pkg(self.pkg_path)

        argv = self.base_argv + ['list', '-F', '{branch}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'master\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

    def test_list_pkg_not_created(self):
        if os.path.exists(self.pkg_path):
            shutil.rmtree(self.pkg_path)

        argv = self.base_argv + ['list', '-F', '{name} {path}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'testpkg {path}\n'.format(path=self.pkg_path),
                         'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{basedir}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '/foo\n',
                         'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{branch}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{changes_file}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{debian_branch} {depends}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual('master ()\n', out, 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{dist}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{distribution}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{dsc_file}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{export_dir}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        expected_path = os.path.join(os.path.dirname(self.pkg_path), 'build-area')
        self.assertEqual(out, '{path}\n'.format(path=expected_path),
                         'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{has_symbols}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'False\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{i}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '0\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{is_dfsg}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{is_native}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{is_merged}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'False\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{last_changelog_distribution}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{last_changelog_version}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{package}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'testpkg\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{source_name}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{source_changes_file}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{upstream_branch}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'upstream\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{upstream_version}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{urgency}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{vcs_git}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{version}', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-F', '{version_at_distribution}',
                                 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertTrue(len(err), 'Unexpected stderr: {}'.format(err))

    def test_list_fields(self):
        # An empty result, no new line
        argv = self.base_argv + ['list', '-f', 'vcs_git', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(out), 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        # Test with a list of fields
        argv = self.base_argv + ['list', '-f', 'name,path package, debian_branch', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out,
                         'testpkg\t{path}\ttestpkg\tmaster\n'.format(
                             path=self.pkg_path),
                         'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

    def test_list_sort(self):
        argv = self.base_argv + ['list', '-s', 'alpha', 'testpkg', 'ztestpkg', 'atestpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'atestpkg\ntestpkg\nztestpkg\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-s', 'raw', 'testpkg', 'ztestpkg', 'atestpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'testpkg\nztestpkg\natestpkg\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['list', '-s', 'build', 'testpkg', 'ztestpkg', 'atestpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'testpkg\natestpkg\nztestpkg\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

    def test_list_groups(self):
        argv = self.base_argv + ['list', '_test_group_1']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'atestpkg\nztestpkg\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        # Test inheritance
        argv = self.base_argv + ['list', '-f', 'export_dir, vcs_git', '_test_group_1']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, '/tmp/atestpkg/export_dir\tgit://atestpkg\n'
                         '/tmp/ztestpkg/export_dir\tgit://ztestpkg\n',
                         'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))


class TestArrieroExec(unittest.TestCase):

    def setUp(self):
        configuration = '''
        [DEFAULT]
        basedir: /foo

        [testpkg]
        path: {path}
        '''
        self.config_file = tempfile.NamedTemporaryFile(
            prefix='arriero.', suffix='.conf')
        self.pkg_path = tempfile.mkdtemp(prefix='arriero.')
        self.config_file.write(
            configuration.format(path=self.pkg_path).encode('utf8')
        )
        self.config_file.flush()
        self.base_argv = ['arriero', '--config', self.config_file.name]
        create_basic_git_pkg(self.pkg_path)

    def tearDown(self):
        self.config_file.close()
        if os.path.exists(self.pkg_path):
            shutil.rmtree(self.pkg_path)

    def test_exec_basic_pkg(self):
        argv = self.base_argv + ['exec', '-x', 'echo \'{name}\'', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'testpkg\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['exec', '-x', 'echo \'{name}\'',
                                 '-x', 'echo \'{basedir}\'', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'testpkg\n/foo\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))

        argv = self.base_argv + ['exec', '-f', 'name', '-x', 'set | grep -a \'^name\'', 'testpkg']
        with patch.object(sys, 'argv', new=argv), \
                patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                               new_callable=io.StringIO) as _sys:
            arriero.main()
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertEqual(out, 'name=testpkg\n', 'Unexpected stdout: {}'.format(out))
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))


if __name__ == '__main__':
    unittest.main()
