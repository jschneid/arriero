# -*- coding: utf8 -*-
# Copyright: 2013-2014, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import collections
import glob
import logging
import os
import re
import subprocess

import debian.debian_support as ds
import lxml.etree

from . import util
from .errors import UscanError

# Taken from gbp.deb.uscan and modified to suit this program
# Copyright: 2012, Guido Günther <agx@sigxcpu.org>
# License: GPL-2+
'''Interface to uscan'''


class Uscan(object):
    cmd = '/usr/bin/uscan'

    class Package(object):
        upstream_version = None
        debian_upstream_version = None

        @property
        def version(self):
            return self.upstream_version \
                if self.upstream_version else self.debian_upstream_version

    def __init__(self, dir='.', destdir='..'):
        self._uptodate = False
        self._tarball = None
        self._version = None
        self._uversion = None
        self._url = None
        self._dir = os.path.abspath(dir)
        self._destdir = destdir

    @property
    def uptodate(self):
        return self._uptodate

    @property
    def tarball(self):
        return self._tarball

    @property
    def version(self):
        return self._version if self._version else self._uversion

    @property
    def uversion(self):
        return self._uversion

    @property
    def url(self):
        return self._url

    def _process_package_check_repacked(self, entry, package, filename):

        m = re.search(r'Successfully repacked (?:.+) as (.+),',
                      entry['messages'])
        if m:
            filename = m.group(1)
            # Use the repacked version
            m = re.match(r'(?:.*)_([^_]+)\.orig\.', filename)
            if m:
                package.upstream_version = m.group(1)
        return filename

    def _process_package_check_symlink(self, entry, filename):

        if not filename:
            m = re.match(r'.*symlinked ([^\s]+) to it', entry['messages'])
            if m:
                filename = m.group(1)
        return filename

    def _process_package_check_downloaded(self, entry, filename):

        if not filename:
            m = re.match(r'Successfully downloaded updated package '
                         r'(.+)', entry['messages'])
            if m:
                filename = m.group(1)
        return filename

    def _process_package_check_orig(self, package, filename):

        def _get_ext(package):

            r = os.path.splitext(package.url)
            if len(r) > 1:
                return r[1]
            r = os.path.splitext(package.tarball)
            if len(r) > 1:
                return r[1]
            return ''

        if not filename:
            ext = _get_ext(package)
            if package.package and package.version and ext:
                filename = '{0.package}_{0.version}.orig.tar{1}'.format(
                    package, ext)
        return filename

    def _process_package_set_tarball(self, package, destdir, filename):

        if filename:
            fullpath = os.path.join(destdir, filename)
            if os.path.exists(fullpath):
                package.tarball = fullpath

        if (not package.tarball) and package.package and package.version:
            filename_glob = '{0.package}_{0.version}.orig.tar.*'.format(
                package)
            wild = os.path.join(destdir, filename_glob)
            files = glob.glob(wild)
            if len(files):
                filename = os.path.basename(files[0])
                package.tarball = files[0]

        if (not package.tarball) and package.url:
            filename = package.url.rsplit('/', 1)[1]
            fullpath = os.path.join(destdir, filename)
            if os.path.exists(fullpath):
                package.tarball = fullpath

        return filename

    def _process_package(self, entry, destdir):

        package = self.Package()
        package.package = entry['package']
        package.status = entry['status']
        package.upstream_version = entry['upstream-version']
        package.debian_upstream_version = entry['debian-uversion']
        package.url = entry['upstream-url']
        package.tarball = entry['target-path']

        filename = entry['target']

        filename = self._process_package_check_repacked(entry, package, filename)

        filename = self._process_package_check_symlink(entry, filename)

        filename = self._process_package_check_downloaded(entry, filename)

        filename = self._process_package_check_downloaded(entry, filename)

        filename = self._process_package_check_orig(package, filename)

        filename = self._process_package_set_tarball(package, destdir, filename)

        package.filename = filename
        return package

    def _parse(self, out, destdir=None):
        r'''
        Parse the uscan output return and update the object's properties

        @param out: uscan output
        @type out: string

        >>> u = Uscan('http://example.com/')
        >>> u._parse('<target>virt-viewer_0.4.0.orig.tar.gz</target>')
        >>> u.tarball
        '../virt-viewer_0.4.0.orig.tar.gz'
        >>> u.uptodate
        False
        >>> u._parse('')
        Traceback (most recent call last):
        ...
        UscanError: Couldn't find 'upstream-url' in uscan output
        >>> u._parse('<dehs><warnings>uscan: no watch file found</warnings></dehs>')
        Traceback (most recent call last):
        ...
        UscanError: Uscan warning: uscan: no watch file found
        '''
        xml_root = lxml.etree.fromstring(out)
        if xml_root.tag != 'dehs':
            raise UscanError(
                'Unexpected uscan output, missing dehs tag: {}'.format(out))
        packages = []
        current = None
        for i, child in enumerate(xml_root):
            # logging.info('_parse: {}'.format(child.tag))
            if child.tag == 'package':
                current = collections.defaultdict(str)
                packages.append(current)
            if current is None:
                if child.tag == 'warnings':
                    raise UscanError('Uscan warning: {}'.format(child.text))
                raise UscanError('Unexpected uscan output: {}'.format(out))
            current[child.tag] = child.text
        # logging.info('%s', str(packages))

        if not destdir:
            destdir = self._destdir

        latest = None
        for entry in packages:
            package = self._process_package(entry, destdir)
            if not latest or \
               ds.version_compare(
                   latest.version, package.version) < 0:
                latest = package

        if not latest:
            raise UscanError('Unexpected uscan output: {}'.format(out))

        self._uptodate = (latest.status == 'up to date')
        self._tarball = latest.tarball
        self._version = latest.debian_upstream_version
        self._uversion = latest.upstream_version
        self._url = latest.url

    def _raise_error(self, out):
        r'''
        Parse the uscan output for errors and warnings and raise
        a L{UscanError} exception based on this. If no error detail
        is found a generic error message is used.

        @param out: uscan output
        @type out: string
        @raises UscanError: exception raised

        >>> u = Uscan('http://example.com/')
        >>> u._raise_error("<warnings>uscan warning: "
        ... "In watchfile debian/watch, reading webpage\n"
        ... "http://a.b/ failed: 500 Cant connect "
        ... "to example.com:80 (Bad hostname)</warnings>")
        Traceback (most recent call last):
        ...
        UscanError: Uscan failed: uscan warning: In watchfile debian/watch, reading webpage
        http://a.b/ failed: 500 Cant connect to example.com:80 (Bad hostname)
        >>> u._raise_error("<errors>uscan: Can't use --verbose if "
        ... "you're using --dehs!</errors>")
        Traceback (most recent call last):
        ...
        UscanError: Uscan failed: uscan: Can't use --verbose if you're using --dehs!
        >>> u = u._raise_error('')
        Traceback (most recent call last):
        ...
        UscanError: Uscan failed - debug by running 'uscan --verbose'
        '''
        msg = None

        for n in ('errors', 'warnings'):
            m = re.search('<{0}>(.*)</{0}>'.format(n), out, re.DOTALL)
            if m:
                msg = 'Uscan failed: {}'.format(m.group(1))
                break

        if not msg:
            msg = "Uscan failed - debug by running 'uscan --verbose'"
        raise UscanError(msg)

    def scan(self, destdir=None, download=True, force_download=False,
             version=None):
        '''Invoke uscan to fetch a new upstream version'''
        if not destdir:
            destdir = self._destdir
        util.ensure_path(destdir)
        cmd = [self.cmd, '--symlink', '--destdir={}'.format(destdir),
               '--dehs', '--watchfile', 'debian/watch']
        if not download:
            cmd.append('--report')
        if download and force_download or download and version:
            cmd.append('--force-download')
        if version:
            cmd.append('--download-version={}'.format(version))

        logging.debug('Calling uscan: {}'.format(cmd))
        p = subprocess.Popen(cmd, cwd=self._dir, universal_newlines=True,
                             stdout=subprocess.PIPE)
        out = p.communicate()[0]
        # uscan exits with 1 in case of uptodate and when an error occured.
        # Don't fail in the uptodate case:
        self._parse(out, destdir)
        if not self.uptodate and p.returncode:
            self._raise_error(out)
        if download and not self._tarball:
            raise UscanError("Couldn't find tarball")


# vi:expandtab:softtabstop=4:shiftwidth=4:smarttab
