#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import configparser
import os
import shutil

from collections import OrderedDict, defaultdict, namedtuple

from . import util

# types: string (default),
#        multistring (split('\n'), each term is expanded),
#        rawstring (do not expand format strings),
#        multiraw (split('\n'), each term is not expanded),
#        multivalue (util.split, not expanded),
#        int, bool (util.str2bool),
#        path (string with os.path.expanduser and normpath)
#        multipath (multistring with os.path.expanduser and normpath)
Schema = namedtuple('Schema', ['type', 'inherit'])
Schema.__new__.__defaults__ = ('string', True)


class Configuration(object):

    'Unified interface to configparser and argparse'

    _schema_types = set(('string', 'multistring', 'rawstring', 'multiraw',
                         'multivalue', 'int', 'bool', 'path', 'multipath'))
    _schema_raw_types = set(('rawstring', 'multiraw', 'multivalue'))

    def __init__(self,
                 defaults=None,
                 schema=None,
                 aliases=None,
                 argparse_kwargs=None,
                 configparser_kwargs=None):

        self.defaults = defaults if defaults else {}
        self.defaults.setdefault('config_files', [])
        self.schema = schema if schema else {}
        self.aliases = aliases if aliases else {}

        argparse_kwargs = argparse_kwargs if argparse_kwargs else {}
        configparser_kwargs = (
            configparser_kwargs if configparser_kwargs else {})

        self.argparser = argparse.ArgumentParser(
            **util.chain_map(argparse_kwargs,
                             fromfile_prefix_chars='@',
                             add_help=False))
        self.argparser.add_argument('-c', '--config',
                                    help='Specify a config file.',
                                    metavar='FILE', action='append',
                                    dest='config_files',
                                    default=list(self.defaults['config_files']))
        self.argparser.add_argument('--with',
                                    help='Override a configuration option.',
                                    nargs=3,
                                    metavar=('SECTION', 'KEY', 'VALUE'),
                                    action='append')
        self.config_files = None  # delay till we have to read the files

        # Let's handle the default section ourselves
        kwargs = util.chain_map(configparser_kwargs, default_section='')
        self.cfgparser = configparser.ConfigParser(**kwargs)
        self._args = None
        self._overrides = None

        self._packages = None
        self._groups = None
        self._parents = None

    @property
    def args(self):
        if not self._args:
            self._args = self.argparser.parse_args()
        return self._args

    @property
    def partial_args(self):
        if not self._args:
            self._args, _ = self.argparser.parse_known_args()
        return self._args

    @property
    def overrides(self):
        if not self._overrides:
            self._overrides = defaultdict(lambda: defaultdict(None))
            overrides = vars(self.partial_args).get('with')
            if overrides:
                for (s, k, v) in overrides:
                    self._overrides[s][k] = v

        return self._overrides

    def add_help(self):
        # Taken from the constructor of argparse
        self.argparser.add_argument(
            '-h', '--help', action='help', default=argparse.SUPPRESS,
            help=argparse._('show this help message and exit'))

    def read_config_files(self):
        if not self.config_files:
            self.config_files = list(
                map(os.path.expanduser, self.partial_args.config_files))
        return self.cfgparser.read(self.config_files)

    def update(self, partial=False):
        self._args = None
        if partial:
            return self.partial_args
        return self.args

    def _get_raw_inherit(self, option, raw, inherit):
        if raw is None:
            raw = (option in self.schema and
                   self.schema[option].type in self._schema_raw_types)
        if inherit is None:
            inherit = (option not in self.schema or
                       self.schema[option].inherit)
        return raw, inherit

    def _get_neighbors(self, name):
        return reversed(self.list_parents(name))

    def _get_maps(self, option, section, raw, inherit, known, instance_dict):

        def visit(section):
            yield self.overrides.get(section, {})
            if section in self.cfgparser:
                d = dict(self.cfgparser.items(section, raw=True))
                yield d
                # compatibility mangling, the keys are mangled into attributes
                # names by argparse, but previous default configurations contained
                # dashes that are now "invalid".
                mangled = option.replace('_', '-')
                if '_' in option and mangled in d:
                    d[option] = d[mangled]
                yield d

        def queue_maps():
            # trivial queries
            yield known
            # configurations of the section
            for mapping in visit(section):
                yield mapping
            # ask the package for internal values like the ones that need to query
            # the files
            yield instance_dict

            # ask the parents
            if inherit:
                # bfs over the parents
                for visit_result in \
                    util.bfs_gen(section,
                                 get_neighbors=self._get_neighbors,
                                 visit=visit):
                    for mapping in visit_result:
                        yield mapping

            # command line options
            yield vars(self.partial_args)

            for mapping in visit('DEFAULT'):
                yield mapping
            yield self.defaults

        return queue_maps()

    def _resolve_aliases(self, field, visited=None):
        if field not in self.aliases:
            return field
        if not visited:
            visited = set()
        if field in visited:
            raise ValueError(
                '{}: Invalid alias {}'.format(self.__class__.__name__, field))
        visited.add(field)
        return self._resolve_aliases(self.aliases[field], visited)

    def _get(self, option, active, default_value=None, section='',
             raw=None, inherit=None, known=None, instance_dict=None):
        'Retrieve the configuration option'

        raw, inherit = self._get_raw_inherit(option, raw, inherit)

        maps = util.CachingIterable(
            self._get_maps(option, section, raw, inherit, known,
                           instance_dict))
        chain = util.ChainMap()
        chain.maps = maps
        chain.default_value = default_value

        lookup = self._resolve_aliases(option)

        value = chain.get(lookup)

        return self._follow_schema(value, option, section, raw, known,
                                   instance_dict, active)

    def get(self, option, default_value=None, section='',
            raw=None, inherit=None, instance=None, **kw):
        'Interpolate values in the configuration value'
        # Initialize known values
        if default_value is None and option in self.defaults:
            default_value = self.defaults[option]
        known = util.chain_map(kw, option=option, section=section,
                               name=section, default_value=default_value)

        instance_dict = None
        if instance and hasattr(instance, '_getter'):
            instance_dict = instance._getter()
        else:
            instance_dict = {}
        # avoid loops in the interpolation
        active = OrderedDict()
        value = self._get(option, active, default_value, section=section,
                          raw=raw, inherit=inherit, known=known,
                          instance_dict=instance_dict)
        return value

    def _follow_schema(self, value, option, section, raw, known,
                       instance_dict, active):
        if value is None:
            return value

        schema = self.schema.get(option, Schema())
        # call the corresponding handler
        attr_name = '_get_{}'.format(schema.type)
        obj = getattr(self, attr_name)
        return obj(value, option, section, raw, known, instance_dict, active)

    def _get_rawstring(self, value, *a, **kw):
        return value

    def _get_multiraw(self, value, *a, **kw):
        if isinstance(value, str):
            value = filter(lambda x: x is not '', value.split('\n'))
        return value

    def _get_string(self, *a, **kw):
        return self._interpolate(*a, **kw)

    def _get_multistring(self, value, *a, **kw):
        value = self._interpolate(value, *a, **kw)
        result = []
        for part in self._get_multiraw(value, *a, **kw):
            aux = self._interpolate(part, *a, **kw)
            if aux:
                result.append(aux)
        return result

    def _get_multivalue(self, value, *a, **kw):
        value = self._interpolate(value, *a, **kw)
        if isinstance(value, str):
            value = util.split(value)
        return value

    def _get_int(self, value, *a, **kw):
        value = self._interpolate(value, *a, **kw)
        return int(value)

    def _get_bool(self, value, *a, **kw):
        value = self._interpolate(value, *a, **kw)
        if isinstance(value, str):
            value = util.str2bool(value)
        return value

    def _get_path(self, value, *a, **kw):
        value = self._interpolate(value, *a, **kw)
        value = os.path.expanduser(value)
        value = os.path.normpath(value)
        return value

    def _get_multipath(self, value, *a, **kw):
        xs = self._get_multistring(value, *a, **kw)
        for i, v in enumerate(xs):
            xs[i] = os.path.expanduser(v)
            xs[i] = os.path.normpath(xs[i])
        return xs

    def _interpolate(self, value, option, section, raw, known,
                     instance_dict, active):

        def learn_fields(required_fields):
            if any(f in active for f in required_fields):
                raise(ValueError('Circular values dependency'))
            for field in required_fields:
                if field in known:
                    continue
                value = self._get(field, active, '', section=section,
                                  known=known, instance_dict=instance_dict)
                if isinstance(value, str):
                    _, fields = util.expansions_needed(value)
                    if fields:
                        value = interpolate(field, value, fields)
                known[field] = value

        def interpolate(option, format_string, required_fields):
            active[option] = (format_string, required_fields)
            learn_fields(required_fields)
            assert(all(f in known for f in required_fields))
            # Apply the format
            value = format_string.format(**known)
            del(active[option])
            return value

        if raw or not isinstance(value, str):
            return value
        # Interpolate the values
        n, fields = util.expansions_needed(value)
        assert(n == 0)
        if fields:
            value = interpolate(option, value, fields)
        return value

    def _init_inheritance(self):
        if self._groups is None:
            self._groups = OrderedDict()
        if self._packages is None:
            self._packages = OrderedDict()
        if self._parents is None:
            self._parents = defaultdict(OrderedDict)

    @property
    def groups(self):
        if self._groups is None:
            self._init_inheritance()
            self.update_inheritance()

        return self._groups.keys()

    @property
    def packages(self):
        if self._packages is None:
            self._init_inheritance()
            self.update_inheritance()

        return self._packages.keys()

    @property
    def parents(self):
        if self._parents is None:
            self._init_inheritance()
            self.update_inheritance()

        return self._parents

    def list_all(self):
        return list(self.groups) + list(self.packages)

    def list_parents(self, name):
        if name in self.parents:
            return self.parents[name].keys()
        return []

    def get_packages(self, group, expanded_groups=None):

        def _get_packages(group, expanded_groups):
            expanded_groups.add(group)

            packages = util.OrderedSet()
            for element in self._groups[group]:
                if element not in self.groups:
                    packages.add(element)
                elif element not in expanded_groups:
                    packages.extend(_get_packages(element, expanded_groups))
            return packages

        if group in self.packages:
            return util.OrderedSet([group])
        if group not in self.groups:
            return util.OrderedSet()

        # Expand package groups in the packages list
        if expanded_groups is None:
            expanded_groups = set()

        return _get_packages(group, expanded_groups)

    def update_inheritance(self):

        def _add(x, to=None, parent=None, value=None):
            to[x] = value
            if parent:
                self._parents[x][parent] = True

        sections = self.cfgparser.sections()
        sections.extend(self.overrides.keys())

        for section in sections:
            if section == 'DEFAULT':
                # The cost of handling the default ourselves
                continue
            section_packages = self.get('packages', section=section, inherit=False)
            # If it contains no packages it's a package
            if section_packages is None:
                _add(section, to=self._packages)
                continue
            # If it contains packages it's a group
            _add(section, to=self._groups, value=section_packages)
            for package in section_packages:
                value = self.get('packages', section=package, inherit=False)
                if value is None:
                    to = self._packages
                else:
                    to = self._groups
                _add(package, to=to, parent=section, value=value)

    def write(self):
        '''Writes any changes to the config file to disk.'''
        config_file = self.config_files[-1]
        if os.path.exists(config_file):
            shutil.copyfile(config_file, config_file + '.bak')

        # Write all the overrides
        for section_name, section in self.overrides.items():
            for option, value in section.items():
                if value is None:
                    continue
                self.cfgparser.set(section_name, option, value)

        with open(config_file, 'w') as f:
            self.cfgparser.write(f)
        self.update_inheritance()

    def arg_add(self, *args, **kwargs):
        return self.argparser.add_argument(*args, **kwargs)

    def add_section(self, *args, **kwargs):
        return self.cfgparser.add_section(*args, **kwargs)

    def set(self, section, option, value):
        self.overrides[section][option] = value
        return value


def main():
    config = Configuration()
    print (config.partial_args)
    config.read_config_files()
    print (config.get('test'))
    print ('{} {}'.format(config.get('test', section='foo')))
    print (config.packages)

if __name__ == '__main__':
    main()
