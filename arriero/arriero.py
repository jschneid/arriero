#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright: 2013-2015, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''
Arriero is a tool for simplifying maintaining many debian packages.
It allows to quickly update, build, push and upload many packages at the same
time.
'''

# System imports
import argparse
import logging
import os
import subprocess
import sys

# Own imports
from . import util
from .actions import AVAILABLE_ACTIONS, ActionError
from .builder import AVAILABLE_TESTERS, AVAILABLE_BUILDERS
from .config import Configuration, Schema
from .graph import TSortGraph
from .package import Package


class ArrieroHandler(logging.FileHandler):

    def __init__(self, logdir, *args, **kwargs):
        self.logdir = logdir
        self.default_target = 'arriero'
        if not os.path.isdir(self.logdir):
            os.makedirs(self.logdir)
        filename = os.path.join(self.logdir,
                                '{}.log'.format(self.default_target))
        super(ArrieroHandler, self).__init__(filename, *args, **kwargs)

    def change_file(self, name=None):
        if not name:
            name = self.default_target
        filename = os.path.join(self.logdir, '{}.log'.format(name))
        self.baseFilename = filename
        if self.stream:
            self.stream.close()
            self.stream = None
        if not self.delay:
            self.stream = self._open()


class Arriero(object):

    '''Main class to interface with the user.

    This class handles the CLI, the config values.  The individual actions are
    delegated to the actions module.
    '''

    defaults = {
        'basedir': '~',
        'builder': 'sbuild',
        'builder_hooks': '',
        'builder_options': '',
        'config_files': ('/etc/arriero.conf', '~/.config/arriero.conf'),
        'cowbuilder_basepath': '/var/cache/pbuilder/base-{dist}-{arch}.cow',
        'debian_branch': 'master',
        'debian_tag': 'debian/{epochless_version}',
        'debian_tags': 'True',
        'depends': '',
        'export_dir': '{path}/../build-area',
        'filter_orig': '',
        'force': 'False',
        'force_orig_source': 'False',
        'ignore_branch': 'False',
        'is_merged': 'False',
        'path': '{basedir}/{name}',
        'pbuilder_basetgz': '/var/cache/pbuilder/{dist}-{arch}-base.tgz',
        'pre_release': 'False',
        'pristine_tar': 'False',
        'run_autopkgtest': 'True',
        'run_lintian': 'True',
        'run_piuparts': 'False',
        'sbuild_chroot': '{dist}-{arch}-sbuild',
        'sbuild_chrootpath': '/var/lib/schroot/chroots/sbuild-{dist}-{arch}',
        'sbuild_test_chroot': '{sbuild_chroot}',
        'source_only': 'False',
        'tarball_dir': '{path}/../tarballs',
        'target_distribution': '{distribution}',
        'tester': 'schroot',
        'tester_name': 'adt-{dist}-{arch}',
        'upload_command': 'dput -u {upload_host} {changes_file}',
        'upload_host': 'local',
        'upstream_branch': 'upstream',
        'upstream_push': 'True',
        'upstream_tag': 'upstream/{upstream_version}',
        'upstream_vcs_tag': '',
        'vcs_git': 'https://anonscm.debian.org/git/collab-maint/{name}.git',
    }
    aliases = {
        'arch': 'architecture',
        'dist': 'distribution',
    }
    schema = {
        'basedir': Schema(type='path'),
        'build_depends': Schema(type='multivalue'),
        'build_file': Schema(type='path'),
        'builder_hooks': Schema(type='multivalue'),
        'builder_options': Schema(type='multistring'),
        'changes_file': Schema(type='path'),
        'config_files': Schema(type='multivalue'),
        'cowbuilder_basepath': Schema(type='path'),
        'depends': Schema(type='multivalue'),
        'debian_tag': Schema(type='rawstring'),
        'debian_tags': Schema(type='bool'),
        'dsc_file': Schema(type='path'),
        'export_dir': Schema(type='path'),
        'filter_orig': Schema(type='multivalue'),
        'force': Schema(type='bool'),
        'force_orig_source': Schema(type='bool'),
        'has_symbols': Schema(type='bool'),
        'i': Schema(type='int'),
        'ignore_branch': Schema(type='bool'),
        'is_dfsg': Schema(type='bool'),
        'is_merged': Schema(type='bool'),
        'is_native': Schema(type='bool'),
        'packages': Schema(type='multivalue', inherit=False),
        'path': Schema(type='path'),
        'pbuilder_basetgz': Schema(type='path'),
        'pre_release': Schema(type='bool'),
        'pristine_tar': Schema(type='bool'),
        'run_autopkgtest': Schema(type='bool'),
        'run_lintian': Schema(type='bool'),
        'run_piuparts': Schema(type='bool'),
        'runtime_depends': Schema(type='multivalue'),
        'sbuild_chrootpath': Schema(type='path'),
        'source_only': Schema(type='bool'),
        'source_changes_file': Schema(type='path'),
        'tarball_dir': Schema(type='path'),
        'tests_depends': Schema(type='multivalue'),
        'upstream_push': Schema(type='bool'),
        'upstream_tag': Schema(type='rawstring'),
        'upstream_vcs_tag': Schema(type='rawstring'),
    }

    def __init__(self):

        self.commands = {}

        # TODO: dynamically read other files and their actions
        self.commands.update(AVAILABLE_ACTIONS)

        self.config = Configuration(
            defaults=self.defaults,
            aliases=self.aliases,
            schema=self.schema,
            argparse_kwargs=util.chain_map(
                description=__doc__,
                formatter_class=argparse.RawTextHelpFormatter,
            )
        )
        self.logger = logging.getLogger()

        self.packages = {}

        self._architecture = None
        self._builder = {}
        self._tester = {}
        self._log_handler = None

        self._add_options()
        read_files = self.config.read_config_files()
        self.config.update(partial=True)

        self._process_log_args()
        for orig_file in self.config.config_files:
            if orig_file not in read_files:
                logging.warning('Could not parse config file: %s', orig_file)

        self.config.update(partial=True)
        self._add_actions_options()
        # Delayed so it doesn't clash the ones in the actions
        self.config.add_help()
        self.config.update()

    # log related
    def add_log_handler(self, handler):
        self._log_handler = handler

    def switch_log(self, to=None):
        if self._log_handler:
            self._log_handler.change_file(name=to)

    @property
    def logdir(self):
        if self._log_handler:
            return self._log_handler.logdir

    def _process_log_args(self):
        self.logger.setLevel(logging.DEBUG)
        console_handler = logging.StreamHandler()
        console_formatter = logging.Formatter('[%(levelname)s] %(message)s')
        console_handler.setFormatter(console_formatter)
        if self.config.get('verbose'):
            console_handler.setLevel(logging.DEBUG)
        elif self.config.get('quiet'):
            console_handler.setLevel(logging.CRITICAL)
        else:
            console_handler.setLevel(logging.WARNING)
        self.logger.addHandler(console_handler)
        if self.config.get('logdir'):
            self._log_handler = ArrieroHandler(
                self.config.get('logdir'))
            file_formatter = logging.Formatter(
                '%(asctime)s [%(levelname)s] %(message)s')
            self._log_handler.setFormatter(file_formatter)
            self._log_handler.setLevel(logging.DEBUG)
            self.logger.addHandler(self._log_handler)

    # arguments/config related
    def show_commands_help(self):
        result = ['\nAvailable commands:']
        commands = sorted(AVAILABLE_ACTIONS.items())
        for command, (classname, helptext) in commands:
            result.append('  %-22s%s.' % (command, helptext))
        return '\n'.join(result)

    def _add_options(self):
        '''Add arguments/config options'''
        self.config.argparser.epilog = self.show_commands_help()

        self.config.arg_add('-v', '--verbose', action='store_true',
                            help='Show more information.')
        self.config.arg_add('-q', '--quiet', action='store_true',
                            help='Show only critical errors.')
        self.config.arg_add('--logdir', default=None,
                            help='Directory to store verbose logs')

    def _add_actions_options(self):
        subparsers = self.config.argparser.add_subparsers(
            title='command', description='valid action',
            help='Command to execute.',
            dest='command',
        )
        for cmd, (cls, _) in self.commands.items():
            subparser = subparsers.add_parser(cmd)
            subparser.set_defaults(cmdcls=cls)
            cls.add_options(self, subparser)

    @property
    def architecture(self):
        if not self._architecture:
            self._architecture = subprocess.check_output(
                ['dpkg-architecture', '-qDEB_BUILD_ARCH'],
                universal_newlines=True).rstrip('\n')
        return self._architecture

    def add_new_package(self, package_name, git_url, path, debian_branch,
                        upstream_branch, pristine_tar):
        '''Adds a new package to the configuration.'''

        if package_name in self.config.list_all():
            logging.error(
                'Package %s definition already in the config file. '
                'Not adding it.', package_name)
            return False

        basedir = self.config.get('basedir')
        basedir = os.path.expanduser(basedir)

        if path.startswith(basedir):
            path = '{basedir}' + path[len(basedir):]

        self.config.add_section(package_name)
        # TODO: Need to make this general, deb-src package have no git repo
        if git_url:
            self.config.set(package_name, 'vcs_git', git_url)
        self.config.set(package_name, 'path', path)
        self.config.set(package_name, 'debian_branch', debian_branch)
        self.config.set(package_name, 'pristine_tar', str(pristine_tar))

        if (upstream_branch):
            self.config.set(package_name, 'upstream-branch', upstream_branch)

        self.config.write()

        return True

    def call_command(self):
        '''Execute the command the user requested.'''
        exit_code = 0
        if 'cmdcls' not in self.config.args:
            self.config.argparser.error('No command specified')
        action = self.config.args.cmdcls
        try:
            instance = action(self)
            success = instance.run()
            exit_code = (0 if success is True else
                         1 if success is False else success)
            instance.print_status()
            return exit_code
        except ActionError as e:
            logging.critical(e)
            return 255

    def get_package(self, name):
        '''Returns a Package object.'''
        if name not in self.packages:
            self.packages[name] = Package(name, self)
        return self.packages[name]

    def sort_by_depends(self, package_names, error=None,
                        binaries=None, get_dependencies=None):

        def _config_depends(package):
            return package.depends

        def _dependencies(package):
            return util.OrderedSet(
                package.internal_dependencies(
                    binaries, get_dependencies(package)).keys()
            )

        def _get_inputs(package_name):
            package = self.get_package(package_name)
            return get_depends(package)

        error = set() if error is None else error

        binaries = (self.prepare_binaries_map(package_names)
                    if binaries is None and get_dependencies is not None else
                    binaries)

        get_depends = (_config_depends if get_dependencies is None else
                       _dependencies)

        graph = TSortGraph(package_names, _get_inputs)
        return graph.sort_generator(skip=error)

    def _get_builder(self, name, distribution, architecture, known, cache):
        distribution = distribution.lower()
        key = (name, distribution, architecture)
        if key in cache:
            return cache[key]

        item = known[name]

        Class = item[0]
        _instance = Class(self, distribution, architecture)
        cache[key] = _instance
        return _instance

    def get_tester(self, tester_name, distribution, architecture):
        '''Get a tester'''
        return self._get_builder(tester_name, distribution, architecture,
                                 AVAILABLE_TESTERS, self._tester)

    def get_builder(self, builder_name, distribution, architecture):
        '''Get a builder'''
        return self._get_builder(builder_name, distribution, architecture,
                                 AVAILABLE_BUILDERS, self._builder)

    def prepare_binaries_map(self, package_names):
        'Map each binpkg with corresponding source.'
        binaries = {}
        for name in package_names:
            package = self.get_package(name)
            for binary_name in package.get_packages():
                binaries[binary_name] = name
        return binaries


def main():
    arriero = Arriero()

    try:
        exit_code = arriero.call_command()
    except argparse.ArgumentError as error:
        logging.error('Error while parsing arguments: %s', error)
        exit_code = 100

    return exit_code


if __name__ == '__main__':
    sys.exit(main())

# vi:expandtab:softtabstop=4:shiftwidth=4:smarttab
