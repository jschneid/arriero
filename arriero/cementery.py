#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import logging
import sys

import git
##
# Misc functions
##


def action_report(msg, action):
    ''' Executes action and prints the success state.
    '''
    print('{}: '.format(msg), end='')
    if action():
        print('done.')
        return True
    else:
        print('fail.')
        return False


def check_report(msg, action):
    if not action():
        print('%s' % (msg,), file=sys.stderr)
        return False
    return True


def git_checkout(repo, branch):
    ''' Switch repo to branch.
    '''
    try:
        repo.git.checkout(branch)
    except git.exc.GitCommandError:
        return False
    return True


def list_module(self, name, order='build'):

    def raw(ps):
        return ps

    def build(ps):
        return self.sort_buildable(ps)

    module = self.get_module(name)
    if order not in set(('raw', 'alpha', 'build')):
        raise ValueError('Invalid value for order')

    s = raw
    if order == 'alpha':
        s = sorted
    elif order == 'build':
        s = build

    for package in s(module.packages):
        print(package)


class Arriero_attic(object):

    # Old commands, no longer used.
    def set_debian_push(self, name):
        module = self.get_module(name)

        # TODO: fix, ugly ugly
        for package_name in module.packages:
            package = self.get_package(package_name)

            try:
                remote = package.git.config('branch.%s.remote' % (
                    package.debian_branch,))
            except git.exc.GitCommandError:
                remote = 'origin'

            try:
                ref = package.git.config('branch.%s.merge' % (
                    package.debian_branch,))
            except git.exc.GitCommandError:
                ref = 'refs/heads/%s' % (package.debian_branch,)

            try:
                package.git.config('--get', 'remote.%s.push' % (remote,),
                                   ref)
            except git.exc.GitCommandError:
                package.git.config('--add', 'remote.%s.push' % (remote,),
                                   ref)

    def checkout_debian(self, name):
        module = self.get_module(name)

        for package_name in module.packages:
            package = self.get_package(package_name)
            if not package.switch_branches(package.debian_branch):
                logging.error('Failure while switching branches for: %s',
                              package_name)
