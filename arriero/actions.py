# -*- coding: utf8 -*-
# Copyright: 2013-2015, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import argparse
import collections
import glob
import itertools
import logging
import os
import re
import subprocess
import tempfile

import debian.deb822 as deb822

# Own imports
from . import util
from .errors import ArrieroError, ActionError
from .package import ERROR, IGNORE, OK
from .version import Version


class Action(object):

    '''Abstract class that all actions should subclass.'''

    def __init__(self, arriero):
        '''Basic constructor for the class that calls the necessary functions.
           It follows the Template Pattern.
        '''
        self._arriero = arriero
        self._process_options()

    @property
    def config(self):
        return self._arriero.config

    def _get_packages(self, names):
        '''Expands the groups in names, returning the package names.'''
        packages = util.OrderedSet()
        groups = self.config.groups
        for name in names:
            if name in groups:
                group_packages = self.config.get_packages(name)
                packages.extend(group_packages)
            else:
                packages.add(name)
        return packages

    @classmethod
    def add_options(cls, arriero, parser):
        '''Add arguments/config options'''

    def _process_options(self):
        '''Process the options after they were parsed.'''

    def run(self):
        '''Execute this action's main goal. Returns True if successful.'''

    def print_status(self):
        '''Print a status report of the run.'''

    def actionpackage_print_status(self):
        log_files = [
            util.AttrDict(
                level=ERROR, filename='error', f=None,
                log=lambda p: logging.error('%s: failed', p)),
            util.AttrDict(
                level=IGNORE, filename='ignore', f=None,
                log=lambda p: logging.info('%s: ignored', p)),
            util.AttrDict(
                level=OK, filename='success', f=None,
                log=lambda p: None),
        ]
        logdir = self._arriero.logdir
        for item in log_files:
            if logdir:
                item.f = open(os.path.join(logdir, item.filename), 'w')
        for item in log_files:
            for package in self._results[item.level]:
                if item.f:
                    item.f.write('{}\n'.format(package))
                item.log(package)
            if item.f:
                item.f.close()

    # Methods related to parsing arguments

    @classmethod
    def _add_option_all(cls, arriero, parser):
        parser.add_argument('-a', '--all', action='store_true')

    # names
    @classmethod
    def _add_option_names(cls, arriero, parser, strict=True):
        if strict:
            names_choices = set(['']) | set(arriero.config.list_all())
        else:
            names_choices = None
        parser.add_argument('names', nargs='*', default='',
                            choices=names_choices)

    def _process_option_names(self):
        if self.config.get('all'):
            self.names = self.config.packages
            return
        self.names = self.config.get('names')
        if not self.names:
            # Try to find out if we are standing on a current package
            cwd = os.getcwd()
            for pkg_name in self.config.packages:
                pkg = self._arriero.get_package(pkg_name)
                if pkg.path == cwd:
                    self.names = [pkg_name]
                    return
            raise argparse.ArgumentError(None, 'No package names received.')

    @classmethod
    def _add_boolean_argument(cls, parser, name, default=False, dest=None):
        '''Helper to add options --name --no-name and set the default'''
        if dest is None:
            dest = name.replace('-', '_')
        parser.add_argument('--{}'.format(name), action='store_true',
                            dest=dest)
        parser.add_argument('--no-{}'.format(name),
                            action='store_false', dest=dest)
        parser.set_defaults(**{dest: default})

    @classmethod
    def _add_option_ignore_branch(cls, arriero, parser):
        cls._add_boolean_argument(parser, 'ignore-branch', default=None)


class ActionPackages(Action):

    def _get_name(self):
        return self.__class__.__name__

    @classmethod
    def add_options(cls, arriero, parser):
        super(ActionPackages, cls).add_options(arriero, parser)
        cls._add_option_all(arriero, parser)
        cls._add_option_names(arriero, parser, strict=True)
        cls._add_option_ignore_branch(arriero, parser)

    def _process_options(self):
        self._process_option_names()

    def run(self):
        '''Method for iterating packages and applying a function.'''
        packages = self._get_packages(self.names)
        self._results = collections.defaultdict(set)

        for package_name in packages:
            self._arriero.switch_log(to=package_name)
            logging.info('%s: executing %s action.',
                         package_name, self._get_name())
            package = self._arriero.get_package(package_name)
            try:
                status = self._package_action(package)
            except ArrieroError as e:
                status = ERROR
                self._arriero.switch_log()
                logging.error('%s: action %s FAILED with %s',
                              package_name, self._get_name(), e)
            else:
                if status == ERROR:
                    self._arriero.switch_log()
                    logging.error('%s: action %s FAILED',
                                  package_name, self._get_name())
            self._results[status].add(package.name)
        self._arriero.switch_log()

        if self._results[ERROR]:
            return False
        return True

    print_status = Action.actionpackage_print_status

    def _package_action(self, package):
        '''To override.'''


class ActionFields(ActionPackages):

    '''Common class for List and Exec'''

    available_fields = set([
        'basedir',
        'branch',
        'build_file',
        'changes_file',
        'debian_branch',
        'depends',
        'dist',
        'distribution',
        'dsc_file',
        'export_dir',
        'has_symbols',
        'i',
        'is_dfsg',
        'is_native',
        'is_merged',
        'last_changelog_distribution',
        'last_changelog_version',
        'name',
        'package',
        'path',
        'source_name',
        'source_changes_file',
        'tarball_dir',
        'upstream_branch',
        'upstream_version',
        'urgency',
        'vcs_git',
        'version',
        'version_at_distribution',
    ])
    available_sorts = set(('raw', 'alpha', 'build'))
    aliases_fields = {
        'dist': 'distribution',
        'package': 'name',
    }

    @classmethod
    def add_options(cls, arriero, parser):
        super(ActionFields, cls).add_options(arriero, parser)
        parser.add_argument('-f', '--fields', default='name')
        parser.add_argument('-s', '--sort', choices=cls.available_sorts,
                            default='raw')

    def _process_options(self, *formats):
        super(ActionFields, self)._process_options()
        self.fields = util.split(self.config.get('fields'))
        self.field_names = {}

        required_fields = []
        for format_string in formats:
            if not format_string:
                continue
            _, f = util.expansions_needed(format_string)
            required_fields.extend(f)

        for field_name in itertools.chain(self.fields, required_fields):
            if field_name not in self.available_fields:
                raise ActionError(
                    'action %s: Field %s not known' % (
                        self._get_name(),
                        field_name
                    )
                )
            self.field_names[field_name] = None

    def _get_field(self, package, field):
        obj = getattr(package, field)
        if hasattr(obj, '__call__'):
            result = obj()
        else:
            result = obj
        return (result if isinstance(result, str) else
                '' if result is None else
                str(tuple(result)) if (
                    isinstance(result, (set,
                                        deb822.OrderedSet,
                                        collections.Set))) else
                str(result))

    def _resolve_aliases(self, field, visited=None):
        if field not in self.aliases_fields:
            return field
        if not visited:
            visited = set()
        if field in visited:
            raise ActionError('action %s: Invalid alias %s' %
                              (self.__class__.__name__, field))
        visited.add(field)
        return self._resolve_aliases(self.aliases_fields[field], visited)

    def _get_field_values(self, package, known=None):
        values = []
        by_name = {}
        for field in self.field_names:
            lookup = self._resolve_aliases(field)
            if known and field in known:
                field_value = known[field]
            else:
                field_value = self._get_field(package, lookup)
            by_name[field] = field_value

        for field in self.fields:
            values.append(by_name[field])

        return values, by_name

    def _get_packages(self, names):
        return self._sorted(super(ActionFields, self)._get_packages(names))

    def run(self):
        '''Method for iterating packages and applying a function.'''
        packages = self._get_packages(self.names)
        self._results = collections.defaultdict(set)

        for i, package_name in enumerate(packages):
            self._arriero.switch_log(to=package_name)
            logging.info('%s: executing %s action.',
                         package_name, self._get_name())
            package = self._arriero.get_package(package_name)
            try:
                if hasattr(self, '_package_action_i'):
                    status = self._package_action_i(package, i)
                else:
                    status = self._package_action(package)
            except ArrieroError as e:
                status = ERROR
                self._arriero.switch_log()
                logging.error('%s: action %s FAILED with %s',
                              package_name, self._get_name(), e)
            else:
                if status == ERROR:
                    self._arriero.switch_log()
                    logging.error('%s: action %s FAILED',
                                  package_name, self._get_name())
            self._results[status].add(package.name)

        if self._results[ERROR]:
            return False
        return True

    # Sort methods
    def raw_sort(self, packages):
        return packages

    def alpha_sort(self, packages):
        return sorted(packages)

    def build_sort(self, packages):
        return self._arriero.sort_by_depends(packages)

    def _sorted(self, packages):
        method_name = self.config.get('sort') + '_sort'
        method = getattr(self, method_name)
        return method(packages)


class List(ActionFields):

    '''Query the available packages with formatting.'''

    @classmethod
    def add_options(cls, arriero, parser):
        super(List, cls).add_options(arriero, parser)
        parser.add_argument('-F', '--format', default=None,
                            dest='output_format')
        parser.add_argument('-e', '--include-empty-results',
                            action='store_true')

    def _process_options(self):
        self.output_format = self.config.get('output_format', raw=True)
        super(List, self)._process_options(self.output_format)
        self.include_empty_results = self.config.get('include_empty_results')

    def _is_empty(self, iterable):
        '''Returns True if the iterables has all empty elements.'''
        if not iterable:
            return True
        for value in iterable:
            if value:
                return False
        return True

    def _package_action_i(self, package, i):
        values, by_name = self._get_field_values(package, {'i': str(i)})

        if self.include_empty_results or not self._is_empty(values):
            if self.output_format:
                print (self.output_format.format(*values, **by_name))
            else:
                print ('\t'.join(values))
        return OK


class Exec(ActionFields):

    '''Run a command on each package.'''

    @classmethod
    def add_options(cls, arriero, parser):
        super(Exec, cls).add_options(arriero, parser)
        parser.add_argument('-x', '--script', action='append')
        parser.add_argument('--no-env',
                            action='store_false',
                            dest='env')
        parser.add_argument('--no-chdir',
                            action='store_false',
                            dest='chdir')

    def _process_options(self):
        self._scripts = self.config.get('script', raw=True)
        super(Exec, self)._process_options(*self._scripts)

    def _package_action_i(self, package, i):
        status = OK
        kwargs = {'interactive': True, 'shell': True}

        if self.config.get('chdir'):
            kwargs['cwd'] = package.path

        values, by_name = self._get_field_values(package, {'i': str(i)})

        if self.config.get('env'):
            kwargs['env'] = dict(os.environ, **by_name)

        for script in self._scripts:
            script_formatted = script.format(*values, **by_name)

            try:
                util.log_check_call(script_formatted, **kwargs)
            except (ArrieroError, subprocess.CalledProcessError) as e:
                logging.error('%s: %s', package.name, e)
                status = ERROR
                break
        return status


class Clone(Action):

    '''Clone upstream repositories.'''

    @classmethod
    def add_options(cls, arriero, parser):
        cls._add_option_all(arriero, parser)
        cls._add_option_names(arriero, parser, strict=False)
        parser.add_argument('--basedir', default=None)
        parser.add_argument('--upstream-branch', default=None)
        parser.add_argument('--debian-branch', default=None)
        parser.add_argument('--vcs-git', default=None)

    def _process_options(self):
        self._process_option_names()

        # Split the names into URLs and packages.
        self._packages = set()
        self._urls = set()
        for name in self.config.get('names'):
            if ':' in name:
                self._urls.add(name)
            else:
                self._packages.add(self._arriero.get_package(name))

    def run(self):
        self._not_ok = set()
        for url in self._urls:
            try:
                if not self.url_clone(url):
                    self._not_ok.add(url)
            except Exception as e:
                logging.error('{}: Failed to clone.'.format(url))
                logging.error('{}: {}'.format(url, e))
                self._not_ok.add(url)

        for package in self._packages:
            self._arriero.switch_log(to=package.name)
            try:
                if not self.package_clone(package):
                    self._not_ok.add(package.name)
            except Exception as e:
                logging.error('{}: Failed to clone.'.format(package.name))
                logging.error('{}: {}'.format(package.name, e))
                self._not_ok.add(package.name)

        # Run status
        if self._not_ok:
            return False
        return True

    def get_remote_heads(self, url):
        heads = set()
        cmd = ['git', 'ls-remote', '--heads', url]
        p = subprocess.Popen(cmd, universal_newlines=True,
                             stdout=subprocess.PIPE)
        for line in p.stdout:
            m = re.search(r'\srefs/heads/(.*)$', line)
            if m:
                heads.add(m.group(1))
        return heads

    def guess_branches(self, url):
        '''Use git ls-remote to check which branches are there.'''
        heads = self.get_remote_heads(url)
        upstream_branch = 'upstream'
        debian_branch = 'master'
        # Review this: Lucky guess?
        if 'debian' in heads:
            debian_branch = 'debian'
            if 'upstream' not in heads:
                if 'master' in heads:
                    upstream_branch = 'master'
        elif 'unstable' in heads:
            debian_branch = 'unstable'
        pristine_tar = False
        if 'pristine-tar' in heads:
            pristine_tar = True

        return debian_branch, upstream_branch, pristine_tar

    def url_clone(self, url):
        '''Clone a package from the provided url.'''

        # Check if this URL is already configured
        for package_name in self.config.packages:
            package = self._arriero.get_package(package_name)
            if package.vcs_git == url:
                logging.warning(
                    'The URL %s is already configured by package %s.',
                    package.vcs_git, package.name)
                logging.warning('Switching to cloning from configuration file.')
                self._packages.add(package)
                return True

        # Get basedir for this package
        basedir = self.config.get('basedir')

        # Guess destdir for this package
        name = os.path.basename(url)
        if name.endswith('.git'):
            name = name[:-4]
        destdir = os.path.join(basedir, name)

        # Guess the branches
        debian_branch, upstream_branch, pristine_tar = self.guess_branches(url)

        self.clone(basedir, destdir, url, debian_branch, upstream_branch,
                   pristine_tar)

        # Obtain package name from control file
        destdir = os.path.expanduser(destdir)
        control_filepath = os.path.join(destdir, 'debian', 'control')
        if not os.path.exists(control_filepath):
            logging.error('Unable to find debian/control while cloning %s', url)
            return False
        control_file = open(control_filepath)
        # Deb822 will parse just the first paragraph, which is ok.
        control = deb822.Deb822(control_file)
        package_name = control['Source']

        if not self._arriero.add_new_package(package_name, url, destdir,
                                             debian_branch, upstream_branch,
                                             pristine_tar):
            logging.error('Clone successful for package not in configuration. '
                          'You will not be able to use arriero with it.')
            return False

        package = self._arriero.get_package(package_name)
        return package.fetch_upstream()

    # TODO: this method should probably be in the package and not here
    def clone(self, basedir, destdir, url, debian_branch, upstream_branch,
              pristine_tar):
        '''Verify the directories for the clone, and clone.'''

        basedir = os.path.expanduser(basedir)
        destdir = os.path.expanduser(destdir)
        util.ensure_path(basedir)
        logging.debug('basedir: %s', basedir)
        logging.debug('destdir: %s', destdir)

        if os.path.exists(destdir):
            logging.error('Cloning %s, directory already exists: %s',
                          url, destdir)
            return False
        dirname, basename = os.path.split(destdir)

        cmd = ['gbp', 'clone']
        if debian_branch:
            cmd.append('--debian-branch=%s' % debian_branch)
        if upstream_branch:
            cmd.append('--upstream-branch=%s' % upstream_branch)
        if pristine_tar:
            cmd.append('--pristine-tar')
        else:
            cmd.append('--no-pristine-tar')
        cmd.append(url)
        cmd.append(basename)

        util.log_check_call(cmd, interactive=True, cwd=dirname)
        logging.info('Successfully cloned %s', url)
        return True

    def package_clone(self, package):
        """Clone a package that is already in the config file."""

        # TODO: shouldn't we check if this returned true or false?
        self.clone(package.basedir, package.path, package.vcs_git,
                   package.debian_branch, package.upstream_branch,
                   package.pristine_tar)

        if package.name not in self.config.list_all():
            success = self._arriero.add_new_package(
                package.name, package.vcs_git, package.path,
                package.debian_branch, package.upstream_branch, package.pristine_tar)

            if not success:
                logging.error('Clone successful for package not in configuration. '
                              'You will not be able to use arriero with it.')

        return package.fetch_upstream()


class Build(ActionPackages):

    '''Merge and compile the received packages.'''
    @classmethod
    def add_options(cls, arriero, parser):
        super(Build, cls).add_options(arriero, parser)
        parser.add_argument('-D', '--distribution', '--dist',
                            default=None, dest='target_distribution')
        parser.add_argument('-A', '--architecture', '--arch',
                            default=None)
        parser.add_argument('-U', '--local-upload', action='store_true')
        parser.add_argument('-S', '--source-only', action='store_true')
        parser.add_argument('-B', '--binary-only', action='store_true')
        cls._add_boolean_argument(parser, 'arch-all', default=True)
        parser.add_argument('--force-orig-source', action='store_true')
        parser.add_argument('-o', '--builder-options', action='append')
        parser.add_argument('--builder', default=None)
        parser.add_argument('--hooks', default=None, dest='builder_hooks')
        parser.add_argument('--run-autopkgtest', default=None)

    def _build_package(self, name):
        package = self._arriero.get_package(name)

        # TODO: why is build catching the exception?
        try:
            package.build()
        except (ArrieroError,
                subprocess.CalledProcessError) as e:
            logging.error('%s: Build failed.', package.name)
            logging.error('%s: %s', package.name, str(e))
            return False

        if package.get('local_upload'):
            try:
                package.local_upload()
            except subprocess.CalledProcessError as e:
                logging.error('%s: Upload failed.', package.name)
                logging.error('%s: %s', package.name, str(e))
                return False

        return True

    def run(self):
        def _get_dependencies(package):
            return package.build_depends

        package_names = self._get_packages(self.names)
        self._results = collections.defaultdict(set)

        for name in self._arriero.sort_by_depends(
                package_names, error=self._results[ERROR],
                get_dependencies=_get_dependencies):
            self._arriero.switch_log(to=name)
            if self._build_package(name):
                self._results[OK].add(name)
                continue

            self._results[ERROR].add(name)
            self._arriero.switch_log()
            logging.error('%s: Build FAILED', name)

        self._results[IGNORE] = ((package_names - self._results[OK]) -
                                 self._results[ERROR])

        if self._results[ERROR]:
            return False
        return True

    print_status = Action.actionpackage_print_status


class Upload(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super(Upload, cls).add_options(arriero, parser)
        parser.add_argument('--host', default='local', dest='upload_host')
        parser.add_argument('-f', '--force', action='store_true')

    def _package_action(self, package):
        try:
            return package.upload()
        except Exception as e:
            logging.error('%s: Failed to upload.', package.name)
            logging.error('%s: %s', package.name, e)
            return ERROR


class Pull(ActionPackages):

    def _package_action(self, package):
        status = OK

        try:
            package.pull()
            logging.info('%s: Successfully pulled.', package.name)
        except ArrieroError as e:
            logging.error('%s: Failed to pull.', package.name)
            logging.error('%s: %s', package.name, e)
            status = ERROR

        return status


class Push(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super().add_options(arriero, parser)
        cls._add_boolean_argument(parser, 'debian-tags', None)

    def _package_action(self, package):
        status = OK
        if not package.switch_branches(package.debian_branch):
            status = ERROR
        else:
            if not package.push():
                status = ERROR
        return status


class Release(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super(Release, cls).add_options(arriero, parser)
        parser.add_argument('-D', '--distribution', default=None)
        parser.add_argument('-P', '--pre-release', action='store_true')

    def _package_action(self, package):

        if not package.switch_branches(package.debian_branch):
            logging.error('%s: can\'t switch to branch %s.',
                          package.name, package.debian_branch)
            return ERROR

        return package.release()


class Update(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super(Update, cls).add_options(arriero, parser)
        parser.add_argument('-V', '--download-version', default=None)

    def _package_action(self, package):
        status = OK
        try:
            package.new_upstream_release()
        except (ArrieroError,
                subprocess.CalledProcessError) as e:
            logging.error('%s: Failed to get new upstream release.',
                          package.name)
            logging.error('%s: %s', package.name, e)
            status = ERROR
        return status


class Status(ActionPackages):

    def _package_action(self, package):
        print ('\n'.join(package.get_status()))


class PrepareOverlay(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super(PrepareOverlay, cls).add_options(arriero, parser)
        parser.add_argument('-b', '--branch', default=None,
                            dest='overlay_branch')

    def _package_action(self, package):
        status = OK
        try:
            package.prepare_overlay(
                overlay_branch=package.get('overlay_branch'))
        except Exception as e:
            logging.error('{}: failure while preparing overlay'.format(
                package.name))
            logging.error('{}: {}'.format(package.name, e))
            status = ERROR
        return status


class FetchUpstream(ActionPackages):

    def _package_action(self, package):
        status = OK
        try:
            package.fetch_upstream()
        except Exception as e:
            logging.error('{}: unable to fetch upstream release'.format(
                          package.name))
            logging.error('{}: {}'.format(package.name, e))
            status = ERROR
        return status


class CheckIfChanged(ActionPackages):

    def _package_action(self, package):
        status = OK
        if package.is_native():
            return status
        current = previous = package.upstream_version
        for block in package.changelog:
            version = Version(str(block.version))
            if version.upstream_version != current:
                previous = version.upstream_version
                break
        if current == previous:
            return status
        changes = package.upstream_changes(old_version=previous)

        if changes:
            msg = 'changes since {}'.format(previous)
        else:
            msg = '{} = {}'.format(current, previous)
        print ('{}: {}'.format(package.name, msg))

        return status


class UpdateSymbols(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super(UpdateSymbols, cls).add_options(arriero, parser)
        parser.add_argument('-D', '--distribution', default=None)
        parser.add_argument('--from-build-log', action='store_true')
        parser.add_argument('--from-buildds-logs', action='store_true')

    @staticmethod
    def _has_symbols_changes(package, since):
        release_tag = package.tag_template('debian', version=since)
        repo = package.repo
        repo_tag = repo.tags[release_tag]
        return bool(repo.index.diff(repo_tag.commit, 'debian/*.symbols'))

    @staticmethod
    def _symbols_helper(package, version, files):
        upstream_version, _ = str(version).split('-')

        cmd = ['pkgkde-symbolshelper', 'batchpatch', '-v', upstream_version]
        cmd.extend(files)
        try:
            util.log_check_call(cmd, cwd=package.path)
        except subprocess.CalledProcessError:
            return None

        # Check index with working dir
        diffs = package.repo.index.diff(None)

        return [diff.a_blob.path for diff in diffs]

    @staticmethod
    def _process_buildd_logs(package, version):
        cmd = ['getbuildlog', package.source_name, version]
        util.log_check_call(cmd, cwd=package.export_dir)
        filename_glob = '{}_{}_*.log*'.format(package.source_name, version)
        downloaded_glob = os.path.join(package.export_dir, filename_glob)
        downloaded_files = glob.glob(downloaded_glob)

        return UpdateSymbols._symbols_helper(package, version, downloaded_files)

    @staticmethod
    def _process_build_log(package):
        files = [package.build_file]
        return UpdateSymbols._symbols_helper(package, package.version, files)

    @staticmethod
    def _has_missing_symbols(package, changes):
        missing_re = re.compile(r'^\s*#(DEPRECATED|MISSING)')
        for filename in changes:
            fullpath = os.path.join(package.path, filename)
            f = open(fullpath)
            for line in f:
                if missing_re.match(line):
                    return True
        return False

    @staticmethod
    def _get_symbols_mtime(package):
        return max(os.path.getmtime(f) for f in package.symbols_files())

    def _buildds_logs_action(self, package):
        status = OK

        # Obtain last distribution
        changelog_dist = self.config.get('distribution')
        if not changelog_dist:
            changelog_dist = package.last_changelog_distribution
        if not changelog_dist:
            logging.info(
                '{}: no previous upload (dist), ignoring.'.format(
                    package.name))
            return IGNORE

        # Obtain last uploaded version
        version = util.version_at_distribution(package.source_name,
                                               changelog_dist)
        if not version:
            logging.info(
                '{}: no previous upload, ignoring.'.format(
                    package.name))
            return IGNORE

        # Check if symbols have changed since last upload
        if self._has_symbols_changes(package, since=version):
            logging.info(
                '{}: there were changes since last upload, ignoring.'.format(
                    package.name))
            return IGNORE

        # Obtain the buildds logs and
        # Update the symbols with the buildds results
        changes = self._process_buildd_logs(package, str(version))
        if not changes:
            logging.info('{}: no changes needed.'.format(package.name))
            return status

        # Check for missing symbols, let the user fix those
        if self._has_missing_symbols(package, changes):
            logging.error('{}: missing symbols'.format(package.name))
            return ERROR

        # Commit the symbols changes
        package.commit(
            msg='Update symbols files from buildds logs ({}).'.format(version),
            files=changes)

        return status

    @staticmethod
    def _build_log_action(package):
        status = OK

        # Obtain build log
        if not package.build_file:
            logging.info('{}: no build file, ignoring.'.format(package.name))
            return IGNORE

        build_mtime = os.path.getmtime(package.build_file)

        # Obtain newer mtime of the symbols files
        symbols_mtime = UpdateSymbols._get_symbols_mtime(package)

        # Skip if symbols mtime is newer than the build log mtime
        if symbols_mtime > build_mtime:
            logging.info('{}: build is previous to the last symbols change'
                         ', ignoring'.format(package.name))
            return IGNORE

        # Process build log
        changes = UpdateSymbols._process_build_log(package)
        if not changes:
            logging.info('{}: no changes needed.'.format(package.name))
            return status

        # Check for missing symbols, let the user fix those
        if UpdateSymbols._has_missing_symbols(package, changes):
            logging.error('{}: missing symbols'.format(package.name))
            return ERROR

        # Commit the symbols changes
        package.commit(msg='Update symbols files.', files=changes)

        return status

    def _package_action(self, package):
        status = OK

        if package.repo.is_dirty(untracked_files=True):
            logging.warning(
                '{}: branch {} has uncommitted changes.'.format(
                    package.name, package.repo.active_branch.name))
            return ERROR

        if not package.has_symbols():
            logging.info(
                '{}: no symbols files, ignoring.'.format(package.name))
            return IGNORE

        buildds = self.config.get('from_buildds_logs')
        build = self.config.get('from_build_log')
        if buildds or build:
            if buildds:
                status = self._buildds_logs_action(package)
            if status != ERROR and build:
                status = self._build_log_action(package)
        else:
            if not package.build_file:
                return self._buildds_logs_action(package)
            return self._build_log_action(package)


class ExternalUploads(ActionPackages):

    @classmethod
    def add_options(cls, arriero, parser):
        super(ExternalUploads, cls).add_options(arriero, parser)
        cls._add_boolean_argument(parser, 'import', dest='do_import')

    def _check_external_uploads(self, package):

        def drop_binnmu_part(version):
            m = re.match('^(.*)\+b[0-9]+$', version)
            if m:
                return m.group(1)
            return version

        status = OK
        archive_versions = util.rmadison(package.source_name)
        pending = collections.defaultdict(set)
        for dist in archive_versions:
            for version in dist:
                version = drop_binnmu_part(version)
                pending[version].add(dist)

        for block in package.changelog:
            if block.version in pending:
                pending.pop(block.version)
        for version, dists in pending.items():
            print ('{}: missing version {} available in {}'.format(
                package.name, version, dists))
        if pending:
            status = ERROR
        return pending, status

    def _import_external_uploads_get_import_dsc_cmd(self, package, tmpdir):
        # gbp import-dsc
        dsc_file = glob.glob('{}/*.dsc'.format(tmpdir))[0]
        cmd = ['gbp', 'import-dsc', dsc_file]
        if package.debian_branch:
            cmd.append('--debian-branch=%s' % package.debian_branch)
        if package.upstream_branch:
            cmd.append('--upstream-branch=%s' % package.upstream_branch)
        if package.pristine_tar:
            cmd.append('--pristine-tar')
            if package.filter_orig:
                cmd.append('--filter-pristine-tar')
                for pattern in package.filter_orig:
                    cmd.append('--filter=%s' % pattern)
        else:
            cmd.append('--no-pristine-tar')
        return cmd

    def _import_external_uploads(self, package):

        pending, status = self._check_external_uploads(package)
        for version, dists in pending.items():
            # Do something
            if 'new' in dists:
                logging.info('{}: ignoring version {} in new'.format(
                    package.name, version))
                dists.remove('new')
                status = IGNORE
            incoming = set()
            for dist in dists:
                if dist.startswith('buildd-'):
                    incoming.add(dist)
            if dists - incoming:
                # In the archive, using debsnap
                with tempfile.TemporaryDirectory(prefix=package.name) as tmpdir:
                    logging.warn('{}: gbp import-dsc always merges the '
                                 'upstream source into the debian branch'.format(
                                     package.name))
                    cmd = ['debsnap', '--destdir={}'.format(tmpdir),
                           '--force', package.source_name, version]
                    util.log_check_call(cmd)
                    cmd = self._import_external_uploads_get_import_dsc_cmd(
                        package, tmpdir)
                    util.log_check_call(cmd)
                status = OK
            elif incoming:
                logging.info('{}: ignoring version {} in incoming'.format(
                    package.name, version))
                status = IGNORE
        return status

    def _package_action(self, package):

        status = OK
        try:
            package.pull()
        except Exception:
            return ERROR

        if not self.config.get('do_import'):
            versions, status = self._check_external_uploads(package)
        else:
            status = self._import_external_uploads(package)
        return status


class InternalDependencies(ActionFields):

    def __init__(self, *args, **kwargs):
        super(InternalDependencies, self).__init__(*args, **kwargs)
        self.binaries = {}
        for package_name in self.config.packages:
            package = self._arriero.get_package(package_name)
            binaries = package.get_packages()
            for binary_name in binaries:
                self.binaries[binary_name] = package_name

    def _package_action(self, package):
        internal_dep = package.internal_dependencies(self.binaries)
        internal_dep = {
            key: {'due_to': value} for (key, value) in internal_dep.items()
        }

        for dep, value in internal_dep.items():
            value['groups'] = \
                sorted(self.config.list_parents(dep))

        nicer = sorted(internal_dep.items(),
                       key=lambda x: (x[1]['groups'], x[0]))

        current_group = None
        groups = []
        for item in nicer:
            if item[1]['groups'] != current_group:
                current_group = item[1]['groups']
                groups.append([])
            groups[-1].append(item[0])
            print ('# {:32} : due to {} ({})'.format(
                item[0], ', '.join(item[1]['due_to']),
                ' '.join(item[1]['groups'])))
        print ('; {}'.format(
            ' '.join(sorted(self.config.list_parents(package.name)))
        ))
        print ('[{}]'.format(package.name))
        print ('depends: ' + '\n         '.join(' '.join(g) for g in groups))
        # print
        # for item in nicer:
        #     print ('{}'.format(item[0]))


# Dictionary containing the available actions in this module and some help
# about what they are for.

AVAILABLE_ACTIONS = {
    'status': (Status, 'Show the status of the package/s'),
    'update': (Update, 'Get the new upstream release of the package/s'),
    'build': (Build, 'Build the package/s in a pbuilder'),
    'list': (List, 'List package/s with a specific format'),
    'exec': (Exec, 'Run commands for each package/s'),
    'clone': (Clone, 'Obtain the repository for the package/s'),
    'upload': (Upload, 'Upload the package/s'),
    'pull': (Pull, 'Pull new changes to the package/s repositories'),
    'push': (Push, 'Push local changes to the package/s repositories'),
    'release': (Release, 'Change the distribution in the changelog'),
    'overlay': (PrepareOverlay, 'Combine upstream and debian branches'),
    'fetch-upstream': (FetchUpstream, 'Fetch upstream tarball'),
    'check-if-changed': (CheckIfChanged, 'Check changes in the upstream tarball'),
    'update-symbols': (UpdateSymbols, 'Update symbols files with previous build'),
    'external-uploads': (ExternalUploads, 'Break stuff'),
    'internal-dependencies': (InternalDependencies, 'List dependencies between packages'),
}

# vi:expandtab:softtabstop=4:shiftwidth=4:smarttab
