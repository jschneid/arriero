#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright: 2013-2014, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import collections

from . import util

# Graph node
Node = collections.namedtuple('Node', ['input', 'output'])


class GraphError(Exception):
    pass


class TSortGraph(object):

    '''Graph for topological sorts.'''

    def __init__(self, keys, get_inputs):
        '''
        :keys: sequence of hashable items.
        :get_inputs: function of key that returns the list of inputs that the
                     node depends on.
        '''
        self.nodes = collections.defaultdict(
            lambda: Node(util.OrderedSet(), util.OrderedSet())
        )
        self.ready = collections.deque()
        self._done = set()
        visited = set()

        node_keys = util.OrderedSet(keys)

        for key in node_keys:
            if key in visited:
                continue
            inputs = get_inputs(key)
            # Reduce the inputs to the set of nodes we are working with
            inputs &= node_keys
            self.nodes[key].input.extend(inputs)
            for input in inputs:
                self.nodes[input].output.add(key)
            if not inputs:
                self.ready.append(key)
            visited.add(key)

    def done(self, key):
        '''Remove key from the inputs of the depending nodes'''
        self._done.add(key)

        for child in self.nodes[key].output:
            self.nodes[child].input.remove(key)
            # if there are no more dependencies, the element is ready
            if not self.nodes[child].input:
                self.ready.append(child)
        return self.ready

    def __repr__(self):
        items = ('{key}: in={node.inputs}, out={node.outputs}'.format(
            key=key, node=node) for key, node in self.nodes.items())
        return '{}({})'.format(self.__class__.__name__, ', '.join(items))

    def sort_generator(self, skip=None):
        '''Topological sort as a generator

        :skip: is a set of values that are not going to be considered as done.
        '''
        while self.ready:
            key = self.ready.popleft()
            yield key
            if skip and key in skip:
                continue
            self.done(key)

        if len(self._done) != len(self.nodes) and not skip:
            raise GraphError('Not a DAG?: done {}, ready {}, graph {}'.format(
                self._done, self.ready, self))
