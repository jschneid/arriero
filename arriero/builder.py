#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright: 2013-2014, Maximiliano Curia <maxy@debian.org>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import getpass
import logging
import os
import re
import shutil
import subprocess
import sys
import tempfile

from pkg_resources import Requirement, resource_filename

from . import util
from .errors import TestFailed

NO = 0
ON_ERROR = 1
ALWAYS = 2


class Common(object):

    def __init__(self, arriero, dist, arch):
        self._arriero = arriero
        self.dist = dist.lower()
        self.arch = arch

        # Update the chroot just once (if we are planning to make this a long
        # running process, then it might make sense to set a ttl.
        self._updated = False

    def update(self, force=False):
        self._updated = True

    @property
    def config(self):
        return self._arriero.config


class Builder(Common):

    def create(self):
        pass

    def build(self, package):
        pass

    def ensure_image(self):
        try:
            self.update()
        except Exception:
            # FIXME: this gets called even if a package fails to be installed
            # we need to avoid calling create if the builder is already
            # created
            # self.create()
            pass

    def get_hooks_dir(self):
        # TODO: use the installed path if installed, is there a way to set a
        # variable after installing?
        dirname = resource_filename(Requirement.parse("Arriero"), "hooks")
        return dirname

    def _get_shell_enum(self, hooks):

        shell = NO
        if 'shell' in hooks:
            shell = ALWAYS
        elif 'shell_on_error' in hooks:
            shell = ON_ERROR
        return shell


class CowBuilder(object):
    updated = set()

    def __init__(self, *a, **kw):
        self.path = None

    def needs_update(self):
        if not self.path:
            return
        return self.path not in self.updated

    def mark_updated(self):
        return self.updated.add(self.path)


class GitPBuilder(Builder, CowBuilder):

    def __init__(self, *args, **kwargs):
        super(GitPBuilder, self).__init__(*args, **kwargs)
        self.path = self.config.get('cowbuilder_basepath')

    def create(self):
        self._updated = True

        env = {'ARCH': self.arch, 'DIST': self.dist}
        if self.dist == 'unreleased':
            options = []
            if 'GIT_PBUILDER_OPTIONS' in os.environ:
                options.append(os.environ['GIT_PBUILDER_OPTIONS'])
            # TODO: Fix this
            options.append('--distribution=unstable')
            ' '.join(options)
            env['GIT_PBUILDER_OPTIONS'] = ' '.join(options)

        logging.warning('Build image for {}-{} not found. Creating...'.format(
            self.dist, self.arch))
        cmd = ['git-pbuilder', 'create']
        util.log_check_call(cmd, interactive=True, env=dict(os.environ, **env))

    def update(self, force=False):
        if not force and self._updated:
            return
        self._updated = True
        if not force and not self.needs_update():
            return
        self.mark_updated()
        env = {'ARCH': self.arch, 'DIST': self.dist}
        cmd = ['git-pbuilder', 'update']
        logging.info('Updating build image for {}-{}'.format(
            self.dist, self.arch))
        util.log_check_call(cmd, interactive=True, env=dict(os.environ, **env))

    def build(self, package, ignore_branch=False):

        env = {}
        cmd, source_only = self._build_cmd(package, ignore_branch)

        tmp_hooks_dir, shell = self._prepare_hooks(package)
        if tmp_hooks_dir:
            gbp_options = []
            if 'GIT_PBUILDER_OPTIONS' in os.environ:
                gbp_options.append(os.environ['GIT_PBUILDER_OPTIONS'])
            gbp_options.append('--hookdir={}'.format(tmp_hooks_dir))
            env['GIT_PBUILDER_OPTIONS'] = ' '.join(gbp_options)

        builder_options = package.get('builder_options')
        if builder_options:
            cmd.extend(builder_options)

        try:
            util.log_check_call(cmd, cwd=package.path,
                                env=dict(os.environ, **env), interactive=True)
        finally:
            if tmp_hooks_dir and os.path.exists(tmp_hooks_dir):
                shutil.rmtree(tmp_hooks_dir)

        if source_only:
            return

        self._run_tests(package, shell)

    def _build_cmd(self, package, ignore_branch):

        cmd = [
            'gbp', 'buildpackage',
            '--git-export-dir={}'.format(package.export_dir),
            '--git-arch={}'.format(self.arch),
            '--git-dist={}'.format(self.dist),
        ]
        force_orig_source = package.get('force_orig_source')
        if force_orig_source:
            cmd.append('-sa')

        source_only = package.get('source_only')
        if source_only:
            cmd.append('-S')
            cmd.append('--git-builder=debuild -I -i -us -uc -d')
        else:
            cmd.append('--git-pbuilder')
            arch_all = package.get('arch_all')
            if not arch_all:
                cmd.append('-B')

        if self.config.get('verbose'):
            cmd.append('--git-verbose')
        if package.pristine_tar:
            cmd.append('--git-pristine-tar')
        # If already generated, use that
        cmd.append('--git-tarball-dir={}'.format(package.export_dir))

        if not ignore_branch:
            cmd.append('--git-debian-branch={}'.format(package.debian_branch))
        if not package.is_merged and not package.is_native():
            cmd.append('--git-overlay')

        return cmd, source_only

    def _prepare_hooks(self, package):
        hooks = package.get('builder_hooks')
        user_hooks_dir = package.get('builder-user-hooks-dir')

        shell = self._get_shell_enum(hooks)

        if not user_hooks_dir and not hooks:
            return None, shell

        tmpdir = tempfile.mkdtemp(prefix='builder', suffix='hooks')
        os.chmod(tmpdir, 0o755)
        if user_hooks_dir:
            cmd = ['cp', '-Lrx', os.path.join(user_hooks_dir, '.'), tmpdir]
            util.quiet(cmd)
        if hooks:
            hooks_dir = self.get_hooks_dir()
            for hook in hooks:
                src = os.path.join(hooks_dir, hook)
                if os.path.isdir(src):
                    cmd = ['cp', '-Lrx', os.path.join(src, '.'), tmpdir]
                    util.quiet(cmd)
                else:
                    # TODO: ERROR ?
                    # ignoring invalid hook
                    logging.warning('{}: Ignoring invalid hook: {}'.format(
                        package.name, hook))
                    pass
        return tmpdir, shell

    def _run_tests(self, package, shell):
        # TODO: launch shell when requested

        if package.get('run_lintian'):
            lintian_cmd = [
                'lintian', '-I', '--pedantic', '--show-overrides',
                package.changes_file]
            util.log_check_call(lintian_cmd, cwd=package.path)
        if package.get('run_piuparts'):
            # --defaults=flavor could use the vendor
            chroot = package.get('cowbuilder_basepath')
            puiparts_cmd = [
                'sudo', 'piuparts', '--arch={}'.format(self.arch),
                '--existing-chroot={}'.format(chroot),
                '--warn-on-debsums-errors',
                package.changes_file]
            if self.dist != 'unreleased':
                # TODO: it will fail if it depends on something from the
                #       users local repositories.
                puiparts_cmd.append('--distribution={}'.format(self.dist))
            util.log_check_call(puiparts_cmd, cwd=package.path)

        run_autopkgtest = (package.get('run_autopkgtest') and
                           package.has_tests())
        if run_autopkgtest:
            tester_name = package.get('tester')
            tester = self._arriero.get_tester(tester_name, self.dist, self.arch)
            tester.update()

            tester.test(package, package.changes_file)


class SChRoot(object):
    updated = set()

    def __init__(self, *a, **kw):
        self.schroot = None

    def needs_update(self):
        if not self.schroot:
            return
        return self.schroot not in self.updated

    def mark_updated(self):
        return self.updated.add(self.schroot)

    def shell_in_session(self, session, start_dir='/'):
        logging.info('Starting shell in session: {} '
                     'directory: {}'.format(session, start_dir))
        if not session:
            # No session, probably it could not even be started
            return
        cmd = ['schroot', '-c', session, '--shell=/bin/bash',
               '--directory={}'.format(start_dir), '-r']
        util.log_check_call(cmd, cwd='/', interactive=True)

    def end_session(self, session):
        if not session:
            # No session, probably it could not even be started
            return
        cmd = ['schroot', '-c', session, '-e']
        util.log_check_call(cmd, interactive=True)


class SBuild(Builder, SChRoot):

    class SBuildSession(object):

        def __init__(self, sbuild, package, cmd):
            self.sbuild = sbuild
            self.package = package
            self.cmd = cmd
            self._session_path = None
            self._build_dir = None
            self.exc_info = None

        def __enter__(self):
            try:
                util.log_check_call(self.cmd,
                                    cwd=self.package.path,
                                    interactive=True)
            except subprocess.CalledProcessError:
                self.exc_info = sys.exc_info()
            return self

        def process_buildlog(self):
            build_dir_re = re.compile(
                r"I: NOTICE: Log filtering will replace '(.*)' with "
                r"'(?:«|<<)PKGBUILDDIR(?:»|>>)'")
            session_path_re = re.compile(
                r"I: NOTICE: Log filtering will replace '(.*)' with "
                r"'(?:«|<<)CHROOT(?:»|>>)'")
            try:
                buildlog = open(self.package.build_file)
            except IOError:
                # No build file, most probably the session could not be
                # started
                return
            for line in buildlog:
                match = session_path_re.match(line)
                if match:
                    self._session_path = match.group(1)
                match = build_dir_re.match(line)
                if match:
                    self._build_dir = match.group(1)
                if self._session_path and self._build_dir:
                    break

        @property
        def session_path(self):
            if not self._session_path:
                self.process_buildlog()
            if self._session_path and not self._session_path.startswith('/'):
                self._session_path = '/' + self._session_path
            return self._session_path

        @property
        def build_dir(self):
            if not self._build_dir:
                self.process_buildlog()
            return self._build_dir

        @property
        def chroot_build_dir(self):
            if self.full_build_dir and os.path.exists(self.full_build_dir):
                return os.path.join('/', self.build_dir)
            return '/'

        @property
        def session(self):
            path = self.session_path
            if path is None:
                return
            return os.path.basename(path)

        @property
        def full_build_dir(self):
            path = self.session_path
            if path is None:
                return
            return os.path.join(self.session_path, self.build_dir)

        def __exit__(self, exc_type, exc_value, traceback):
            self.sbuild.end_session(self.session)

    def __init__(self, arriero, dist, arch, *args, **kwargs):
        super(SBuild, self).__init__(arriero, dist, arch, *args, **kwargs)
        self.schroot = self.config.get('sbuild_chroot', distribution=dist,
                                       architecture=arch)

    def create(self):
        self._updated = True

        # TODO: remove hardcoded mirror
        chrootpath = self.config.get('sbuild_chrootpath')

        if self.dist == 'unreleased':
            distribution = 'unstable'
        else:
            distribution = self.dist

        cmd = ['sudo', 'sbuild-createchroot', '--arch={}'.format(self.arch),
               distribution, chrootpath,
               'http://http.debian.net/debian']
        # TODO: fix image:
        #  - add local repositories
        #  - set fstab pkg_archive / apt-cacher-ng
        #  - configure ccache
        #  - configure tmpfs

        util.log_check_call(cmd, interactive=True)

    def update(self, force=False):
        if not force and self._updated:
            return
        self._updated = True
        if not force and not self.needs_update():
            return
        self.mark_updated()
        cmd = ['sudo', 'sbuild-update', '-uagdr', self.schroot]
        util.log_check_call(cmd, interactive=True)

    def list_missing_hook(self, builddir):
        hook = 'list-missing'
        hooks_dir = self.get_hooks_dir()
        src = os.path.join(hooks_dir, hook)
        with tempfile.TemporaryDirectory(prefix=hook) as tmpdir:
            logging.debug('src: {}, dst: {}'.format(src, tmpdir))
            cmd = ['cp', '-Lrx', os.path.join(src, '.'), tmpdir]
            util.log_check_call(cmd)
            cmd = [os.path.join(tmpdir, hook)]
            process = util.log_popen(cmd, stdout=subprocess.PIPE, cwd=builddir)
            first_line = '=== Start list-missing'
            last_line = '=== End list-missing'
            end = False
            num_files = 0
            for i, line in enumerate(process.stdout):
                print (line),
                if end or (i == 0 and line.startswith(first_line)):
                    pass
                elif not end and line.startswith(last_line):
                    # The doors are playing somewhere
                    end = True
                else:
                    num_files += 1
            returncode = process.wait()
            if returncode or num_files > 0:
                raise TestFailed(
                    "Hook {} failed, returned {}, num of files: {}".format(
                        hook, returncode, num_files))

    def check_lintian(self, package):
        error_re = re.compile(r'Lintian: fail')
        try:
            buildlog = open(package.build_file)
            for line in buildlog:
                match = error_re.match(line)
                if match:
                    raise TestFailed("Lintian failed")
        except Exception:
            raise TestFailed("Couldn't open build log")

    def run_sbuild(self, package, builder_options,
                   ignore_branch, binary_only, arch_all):

        sbuild_cmd = self._run_sbuild_sbuild_cmd(package, arch_all,
                                                 binary_only, builder_options)

        gbp_cmd = [
            'gbp', 'buildpackage',
            '--git-export-dir={}'.format(package.export_dir),
            '--git-arch={}'.format(self.arch),
            '--git-dist={}'.format(self.dist),
        ]

        if self.config.get('verbose'):
            gbp_cmd.append('--git-verbose')
        if package.pristine_tar:
            gbp_cmd.append('--git-pristine-tar')
            # If already generated, use that
            gbp_cmd.append('--git-tarball-dir={}'.format(package.export_dir))
        else:
            gbp_cmd.append('--git-tarball-dir={}'.format(package.tarball_dir))

        if not ignore_branch:
            gbp_cmd.append('--git-debian-branch={}'.format(
                package.debian_branch))
        if not package.is_merged and not package.is_native():
            gbp_cmd.append('--git-overlay')

        gbp_cmd.append('--git-builder={}'.format(' '.join(sbuild_cmd)))

        return self.SBuildSession(self, package, gbp_cmd)

    def build(self, package, ignore_branch=False):

        source_only = package.get('source_only')
        if source_only:
            git_pbuilder = GitPBuilder(self._arriero, self.dist, self.arch)
            git_pbuilder.build(package, ignore_branch)
            return

        run_autopkgtest = (package.get('run_autopkgtest') and
                           package.has_tests())

        hooks = package.get('builder_hooks')
        shell = self._get_shell_enum(hooks)

        builder_options = package.get('builder_options')
        binary_only = package.get('binary_only')
        arch_all = package.get('arch_all')

        with self.run_sbuild(package, builder_options,
                             ignore_branch, binary_only, arch_all) as session:
            exc_info = session.exc_info

            try:
                if exc_info:
                    raise exc_info[0].with_traceback(exc_info[1], exc_info[2])
                self._build_run_tests(package, session,
                                      run_autopkgtest,
                                      run_listmissing='list-missing' in hooks,
                                      run_lintian=package.get('run_lintian'))
            except (subprocess.CalledProcessError, TestFailed):
                if shell in (ON_ERROR, ALWAYS):
                    self.shell_in_session(session.session,
                                          start_dir=session.chroot_build_dir)
                raise

            if shell == ALWAYS:
                self.shell_in_session(session.session,
                                      start_dir=session.chroot_build_dir)

    def _run_sbuild_sbuild_cmd(self, package, arch_all, binary_only,
                               builder_options):

        sbuild_cmd = [
            'sbuild',
            '--dist={}'.format(self.dist),
            '--arch={}'.format(self.arch),
            '--chroot={}'.format(self.schroot),
        ]
        if arch_all:
            sbuild_cmd.append('--arch-all')
        else:
            sbuild_cmd.append('--no-arch-all')

        if not binary_only:
            sbuild_cmd.append('--source')

        if self.config.get('verbose'):
            sbuild_cmd.append('--verbose')

        if package.get('run_lintian'):
            sbuild_cmd.extend([
                '--run-lintian',
                '--lintian-opts="-I --pedantic --show-overrides"'])
        else:
            sbuild_cmd.append('--no-run-lintian')

        if package.get('run_piuparts'):
            sbuild_cmd.append('--run-piuparts')
            piuparts_opts = '--schroot=chroot:{}'.format(self.schroot)
            if self.dist != 'unreleased':
                piuparts_opts = '-d {} {}'.format(self.dist, piuparts_opts)
            sbuild_cmd.append('--piuparts-opts="{}"'.format(piuparts_opts))
        else:
            sbuild_cmd.append('--no-run-piuparts')

        sbuild_cmd.append('--purge=never')

        sbuild_cmd.extend(builder_options)

        return sbuild_cmd

    def _build_run_tests(self, package, session,
                         run_autopkgtest, run_listmissing, run_lintian):

        if run_autopkgtest:
            tester_name = package.get('tester')
            tester = self._arriero.get_tester(tester_name, self.dist, self.arch)
            tester.update()

            tester.test(package, package.changes_file)
            # FIXME: starting a shell in adt send the current terminal
            #        to the background
            # tester.test(package, ON_ERROR if shell else NO)
        if run_listmissing:
            self.list_missing_hook(session.full_build_dir)
        if run_lintian:
            self.check_lintian(package)


class Tester(Common):

    def test(self, package, shell):
        pass


class CowBuilderTester(Tester, CowBuilder):

    def __init__(self, arriero, dist, arch):
        super(CowBuilderTester, self).__init__(arriero, dist, arch)

        self.path = self.config.get('tester_name', distribution=dist,
                                    architecture=arch)

    def update(self, force=False):
        if not force and self._updated:
            return
        self._updated = True
        env = {'ARCH': self.arch, 'DIST': self.dist}
        cmd = ['git-pbuilder', 'update']
        logging.info('Updating build image for {}-{}'.format(
            self.dist, self.arch))
        util.log_check_call(cmd, env=dict(os.environ, interactive=True, **env))

    def test(self, package, changes_file, shell=NO):
        cmd = ['sudo', 'autopkgtest', '-U', changes_file,
               '--user={}'.format(getpass.getuser())]
        if shell:
            cmd.append('--shell-fail')
        if shell == ALWAYS:
            cmd.append('--shell')
        cmd.extend(['--', 'chroot', self.path])
        util.log_check_call(cmd, interactive=True, cwd=package.path)


class LXCTester(Tester):

    def __init__(self, arriero, dist, arch):
        super(LXCTester, self).__init__(arriero, dist, arch)

        self.name = self.config.get('tester_name', distribution=dist,
                                    architecture=arch)

    def update(self, force=False):
        if not force and self._updated:
            return
        self._updated = True
        # TODO: The update script needs to be generated first
        cmd = ['sudo', 'lxc-start', '-n', self.name, '-F', '--',
               '/usr/local/sbin/update']
        util.log_check_call(cmd, interactive=True)

    def test(self, package, changes_file, shell=NO):
        cmd = ['sudo', 'autopkgtest', '-U', changes_file,
               '--user={}'.format(getpass.getuser())]
        if shell:
            cmd.append('--shell-fail')
        if shell == ALWAYS:
            cmd.append('--shell')
        cmd.extend(['--', 'lxc', self.name])
        util.log_check_call(cmd, interactive=True, cwd=package.path)


class SChRootTester(Tester, SChRoot):

    def __init__(self, arriero, dist, arch):
        super(SChRootTester, self).__init__(arriero, dist, arch)

        self.schroot = self.config.get('sbuild_test_chroot',
                                       distribution=dist, architecture=arch)

    def update(self, force=False):
        if not force and self._updated:
            return
        self._updated = True
        if not force and not self.needs_update():
            return
        self.mark_updated()
        cmd = ['sudo', 'sbuild-update', '-uagdr', self.schroot]
        util.log_check_call(cmd, interactive=True)

    def test(self, package, changes_file, shell=NO):
        cmd = ['sudo', 'autopkgtest', '-U', changes_file,
               '--user={}'.format(getpass.getuser())]
        if shell:
            cmd.append('--shell-fail')
        if shell == ALWAYS:
            cmd.append('--shell')
        cmd.extend(['--', 'schroot', self.schroot])
        util.log_check_call(cmd, interactive=True, cwd=package.path)


AVAILABLE_BUILDERS = {
    'git-pbuilder': (GitPBuilder, 'Use git-pbuilder cowbuilder'),
    'sbuild':       (SBuild,      'Use sbuild'),
}

AVAILABLE_TESTERS = {
    'cowbuilder':   (CowBuilderTester, 'Use cowbuilder'),
    'lxc':          (LXCTester,        'Use lxc'),
    'schroot':      (SChRootTester,    'Use schroot'),
}
