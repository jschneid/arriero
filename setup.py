#!/usr/bin/env python3

from setuptools import setup, find_packages
import glob
import os


def find_files(directory, target=None):
    ''' Recursively search for files, return value is in data_files format

    data_files expects a list of tuples, each tuple containing directory, and
    a list of files to copy to that directory.
    '''
    files = []
    if not target:
        target = directory
    for path, directories, filenames in os.walk(directory):
        path = path.replace(directory, target)
        files.append((path,
                      [os.path.join(path, filename)
                       for filename in filenames]))
    return files


setup(
    name='Arriero',
    version='0.7',
    author='Maximiliano Curia',
    author_email='maxy@debian.org',
    description='Arriero Package Helper',
    long_description='''
    Arriero is a tool that allows simplifying the management of *Debian*
    packages, particularly useful when having to make new upstream releases,
    builds and uploads of similar packages.

    It relies heavily in the use of *git-buildpackage* and general *git*
    practices, so it's only useful for packages currently maintained through
    git.
    ''',
    url='http://anonscm.debian.org/git/collab-maint/arriero.git',
    license='GPLv2+',
    requires=['debian', 'git', 'lxml', 'pexpect'],
    classifiers=[
        'Environment :: Console',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Version Control :: Git',
        'Operating System :: POSIX :: Linux',
    ],
    packages=find_packages(exclude=['tests', 'tests.*']),
    entry_points={
        'console_scripts': ['arriero = arriero.arriero:main'],
    },
    data_files=[
        ('scripts', glob.glob('scripts/*')),
        ('examples', glob.glob('examples/*')),
    ] + find_files('hooks'),
    test_suite='tests',
)
